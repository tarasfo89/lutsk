"use strict";
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var nativescript_ng2_parallax_1 = require("nativescript-ng2-parallax/nativescript-ng2-parallax");
var app_component_1 = require("./app.component");
var data_service_1 = require("./services/data.service");
var nativescript_ng2_fonticon_1 = require("nativescript-ng2-fonticon");
var element_registry_1 = require("nativescript-angular/element-registry");
var map = require("nativescript-mapbox");
element_registry_1.registerElement("Mapbox", function () { return map.Mapbox; });
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [app_component_1.AppComponent, nativescript_ng2_parallax_1.ParallaxView,],
        bootstrap: [app_component_1.AppComponent],
        imports: [nativescript_module_1.NativeScriptModule,
            nativescript_ng2_fonticon_1.TNSFontIconModule.forRoot({
                'fa': './fonts/font-awesome.min.css'
            }),
        ],
        schemas: [core_1.NO_ERRORS_SCHEMA],
        providers: [data_service_1.DataService]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUEyRDtBQUMzRCxnRkFBOEU7QUFDOUUsaUdBQW1GO0FBQ25GLGlEQUErQztBQUMvQyx3REFBc0Q7QUFDdEQsdUVBQThEO0FBQzlELDBFQUF3RTtBQUN4RSxJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztBQUN6QyxrQ0FBZSxDQUFDLFFBQVEsRUFBRSxjQUFNLE9BQUEsR0FBRyxDQUFDLE1BQU0sRUFBVixDQUFVLENBQUMsQ0FBQztBQWE1QyxJQUFhLFNBQVM7SUFBdEI7SUFBeUIsQ0FBQztJQUFELGdCQUFDO0FBQUQsQ0FBQyxBQUExQixJQUEwQjtBQUFiLFNBQVM7SUFYckIsZUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFLENBQUMsNEJBQVksRUFBRSx3Q0FBWSxFQUFHO1FBQzVDLFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7UUFDekIsT0FBTyxFQUFFLENBQUMsd0NBQWtCO1lBQzFCLDZDQUFpQixDQUFDLE9BQU8sQ0FBQztnQkFDeEIsSUFBSSxFQUFFLDhCQUE4QjthQUNyQyxDQUFDO1NBQ0Q7UUFDSCxPQUFPLEVBQUUsQ0FBQyx1QkFBZ0IsQ0FBQztRQUMzQixTQUFTLEVBQUUsQ0FBQywwQkFBVyxDQUFDO0tBQ3pCLENBQUM7R0FDVyxTQUFTLENBQUk7QUFBYiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XG5pbXBvcnQgeyBQYXJhbGxheFZpZXcgfSBmcm9tICduYXRpdmVzY3JpcHQtbmcyLXBhcmFsbGF4L25hdGl2ZXNjcmlwdC1uZzItcGFyYWxsYXgnO1xuaW1wb3J0IHsgQXBwQ29tcG9uZW50IH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgeyBUTlNGb250SWNvbk1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZzItZm9udGljb24nO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeSc7XG52YXIgbWFwID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1tYXBib3hcIik7XG5yZWdpc3RlckVsZW1lbnQoXCJNYXBib3hcIiwgKCkgPT4gbWFwLk1hcGJveCk7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0FwcENvbXBvbmVudCwgUGFyYWxsYXhWaWV3LCBdLFxuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIFROU0ZvbnRJY29uTW9kdWxlLmZvclJvb3Qoe1xuICAgICAgJ2ZhJzogJy4vZm9udHMvZm9udC1hd2Vzb21lLm1pbi5jc3MnXG4gICAgfSksXG4gICAgXSxcbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdLFxuICBwcm92aWRlcnM6IFtEYXRhU2VydmljZV1cbn0pXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuIl19