"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var data_service_1 = require("./services/data.service");
var geo = require("nativescript-geolocation");
var nativescript_material_showcaseview_1 = require("nativescript-material-showcaseview");
var elementRegistryModule = require("nativescript-angular/element-registry");
var Toast = require("nativescript-toast");
var platform = require("platform");
var nativescript_ng2_fonticon_1 = require("nativescript-ng2-fonticon");
var application = require("application");
var dialogs = require("ui/dialogs");
elementRegistryModule.registerElement("CardView", function () { return require("nativescript-cardview").CardView; });
elementRegistryModule.registerElement("Gif", function () { return require("nativescript-gif").Gif; });
var AppComponent = (function () {
    function AppComponent(page, data, fonticon) {
        var _this = this;
        this.page = page;
        this.data = data;
        this.fonticon = fonticon;
        this.lang = 'uk';
        this.geoWatcher = null;
        this.mapHeight = platform.screen.mainScreen.heightPixels >= 1280 ? 400 : platform.screen.mainScreen.heightPixels * 0.33;
        this.dataLoaded = false;
        this.itemImageLoaded = false;
        this.audioWorker = new Worker('./workers/audio-player');
        this.audioIsPlaying = false;
        this.audioIsPaused = false;
        this.audioProgressValue = 0;
        page.actionBarHidden = true;
        this.distanceTemplate = this.data.distanceTemplate;
        this.distanceToItem = this.distanceTemplate.distanceToItem[this.lang];
        this.data.loadData(function (isDataLoaded) { _this.dataLoaded = isDataLoaded; });
        this.showcaseView = new nativescript_material_showcaseview_1.NSMaterialShowcaseView();
        this.data.itemsSubject.subscribe(function (res) {
            _this.currentItem = _this.data.getFirstItem();
            _this.handleItemImage();
            _this.drawOnMapInit();
        });
        application.on(application.suspendEvent, function (args) {
            _this.resetPlayer();
        });
        this.audioWorker.onmessage = function (msg) {
            if ('currentProgress' in msg.data) {
                _this.audioProgressValue = msg.data.currentProgress;
            }
        };
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var options = {
            title: "Select language",
            message: "Choose your language",
            // cancelButtonText: "Cancel",
            actions: ["English", "Ukrainian"]
        };
        dialogs.action(options).then(function (result) {
            _this.lang = result === 'Ukrainian' ? 'uk' : 'en';
            _this.showHelp();
        });
    };
    AppComponent.prototype.prevItem = function () {
        this.changeItem(this.data.getPrevItem());
    };
    AppComponent.prototype.nextItem = function () {
        this.changeItem(this.data.getNextItem());
    };
    AppComponent.prototype.changeItem = function (newItem) {
        this.itemImageLoaded = false;
        this.currentItem = newItem;
        this.handleItemImage();
        this.resetPlayer();
        this.showDistanceToItem();
        this.animateToItem();
    };
    AppComponent.prototype.handleItemImage = function () {
        var _this = this;
        if (this.data.fileExists(this.currentItem.image)) {
            this.currentItemImage = this.data.getFullFilePath(this.currentItem.image);
            this.itemImageLoaded = true;
        }
        else {
            this.data.downloadFile(this.currentItem.image, function (file) {
                _this.currentItemImage = file;
                _this.itemImageLoaded = true;
            });
        }
    };
    AppComponent.prototype.onMapReady = function (args) {
        var _this = this;
        this.map = args.map;
        geo.enableLocationRequest().then(function (res) {
            if (geo.isEnabled()) {
                _this.geoWatcher = setInterval(function () { return _this.showDistanceToItem; }, 60 * 1000);
            }
        });
    };
    AppComponent.prototype.drawOnMapInit = function () {
        var _this = this;
        if (this.map) {
            var onCalloutTap = function (marker) {
                alert("Marker callout tapped with title: '" + marker.title + "'");
            };
            this.map.addMarkers(this.data.items.map(function (v) {
                return {
                    lat: v.coords.lat,
                    lng: v.coords.lng,
                    title: v.title[_this.lang],
                };
            }));
            this.showDistanceToItem();
            this.drawRoute();
            this.map.setTilt({
                tilt: 90,
                duration: 3000 // default 5000 (milliseconds)
            });
        }
    };
    AppComponent.prototype.animateToItem = function () {
        this.map.animateCamera({
            target: {
                lat: this.currentItem.coords.lat,
                lng: this.currentItem.coords.lng,
            },
            zoomLevel: 17,
            altitude: 2000,
            bearing: 270,
            tilt: 50,
            duration: 1000 // in milliseconds
        });
    };
    AppComponent.prototype.drawRoute = function () {
        this.map.addPolyline({
            id: 1,
            color: '#336699',
            width: 7,
            points: this.data.route
        });
    };
    AppComponent.prototype.showDistanceToItem = function () {
        var _this = this;
        try {
            if (geo.isEnabled()) {
                geo.getCurrentLocation({ desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000 })
                    .then(function (currentLocation) {
                    var itemLocation = {
                        latitude: _this.currentItem.coords.lat,
                        longitude: _this.currentItem.coords.lng,
                        altitude: 181,
                        horizontalAccuracy: 10,
                        verticalAccuracy: 10,
                        speed: 2,
                        direction: 0,
                        timestamp: new Date(),
                        android: null,
                        ios: null
                    };
                    var distanceInMeters = geo.distance(currentLocation, itemLocation);
                    // Toast.makeText("distanceInMeters = " +distanceInMeters).show();
                    _this.distanceToItem = "" + (distanceInMeters > 1001 ?
                        +(distanceInMeters / 1000).toFixed(2)
                            + " " + _this.distanceTemplate.km[_this.lang] : distanceInMeters.toFixed(0)
                        + " " + _this.distanceTemplate.m[_this.lang]);
                })
                    .catch(function (error) {
                    console.log("getCurrentLocation error:", error);
                });
            }
        }
        catch (e) {
            Toast.makeText(JSON.stringify(e)).show();
        }
    };
    AppComponent.prototype.showHelp = function () {
        this.showcaseView.createSequence([
            {
                target: this.page.getViewById("map"),
                dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
                contentText: this.lang === "uk" ? "Це карта, де показані туристичні об'єкти маршруту" : "This is map, where tourist items will be shown.",
                withRectangleShape: false
            },
            // {
            // 	target: this.page.getViewById("play_button"),
            // 	dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
            // 	contentText: this.lang === "uk" ? "Ця кнопка дозволяє прослухати аудіогід." : "This button let you listen audioguide.",
            // 	withRectangleShape: false
            // },
            {
                target: this.page.getViewById("content"),
                dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
                contentText: this.lang === "uk" ? "Тут відображається інформація про туристичний об'єкт." : "Here you can see info about tourist item.",
                withRectangleShape: false
            },
        ]);
        this.showcaseView.startSequence();
        // this.showcaseView.reset();
    };
    AppComponent.prototype.playPause = function () {
        var _this = this;
        if (!this.audioIsPlaying && !this.audioIsPaused) {
            this.data.getAudioUrl(this.currentItem.audio[this.lang], function (url) {
                _this.audioIsPlaying = true;
                _this.audioWorker.postMessage({ url: url, action: "play" });
            });
        }
        else if (!this.audioIsPlaying && this.audioIsPaused) {
            this.audioWorker.postMessage({ action: "resume" });
            this.audioIsPlaying = true;
            this.audioIsPaused = false;
        }
        else if (this.audioIsPlaying) {
            this.audioWorker.postMessage({ action: "pause" });
            this.audioIsPlaying = false;
            this.audioIsPaused = true;
        }
    };
    AppComponent.prototype.resetPlayer = function () {
        this.audioIsPlaying = false;
        this.audioIsPaused = false;
        this.audioProgressValue = 0;
        this.audioWorker.postMessage({ action: "reset" });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        templateUrl: './app.component.html',
    }),
    __metadata("design:paramtypes", [page_1.Page, data_service_1.DataService, nativescript_ng2_fonticon_1.TNSFontIconService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUFrRDtBQUVsRCxnQ0FBK0I7QUFFL0Isd0RBQXNEO0FBQ3RELDhDQUFnRDtBQUNoRCx5RkFBNEU7QUFDNUUsNkVBQStFO0FBQy9FLDBDQUE0QztBQUM1QyxtQ0FBcUM7QUFJckMsdUVBQStEO0FBQy9ELHlDQUEyQztBQUMzQyxvQ0FBc0M7QUFHdEMscUJBQXFCLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUMsUUFBUSxFQUF6QyxDQUF5QyxDQUFDLENBQUM7QUFDbkcscUJBQXFCLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsR0FBRyxFQUEvQixDQUErQixDQUFDLENBQUM7QUFPcEYsSUFBYSxZQUFZO0lBa0J4QixzQkFBb0IsSUFBVSxFQUFVLElBQWlCLEVBQVUsUUFBNEI7UUFBL0YsaUJBbUJDO1FBbkJtQixTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQW9CO1FBakIvRixTQUFJLEdBQVcsSUFBSSxDQUFDO1FBR3BCLGVBQVUsR0FBUSxJQUFJLENBQUM7UUFJdkIsY0FBUyxHQUFXLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFlBQVksSUFBSSxJQUFJLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDM0gsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixvQkFBZSxHQUFZLEtBQUssQ0FBQztRQUdqQyxnQkFBVyxHQUFHLElBQUksTUFBTSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDbkQsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFDL0IsdUJBQWtCLEdBQVcsQ0FBQyxDQUFDO1FBRzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ25ELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxZQUFZLElBQU8sS0FBSSxDQUFDLFVBQVUsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksMkRBQXNCLEVBQUUsQ0FBQztRQUNqRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQyxHQUFHO1lBQ3BDLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUM1QyxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDdkIsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQUMsSUFBc0M7WUFDL0UsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsVUFBQyxHQUFHO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7WUFDcEQsQ0FBQztRQUNGLENBQUMsQ0FBQTtJQUNGLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBQUEsaUJBV0M7UUFWQSxJQUFJLE9BQU8sR0FBRztZQUNiLEtBQUssRUFBRSxpQkFBaUI7WUFDeEIsT0FBTyxFQUFFLHNCQUFzQjtZQUMvQiw4QkFBOEI7WUFDOUIsT0FBTyxFQUFFLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQztTQUNqQyxDQUFDO1FBQ0YsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFjO1lBQzNDLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxLQUFLLFdBQVcsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pELEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELCtCQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsaUNBQVUsR0FBVixVQUFXLE9BQVk7UUFDdEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7UUFDM0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELHNDQUFlLEdBQWY7UUFBQSxpQkFVQztRQVRBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzdCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLFVBQUMsSUFBSTtnQkFDbkQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDN0IsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7UUFDSixDQUFDO0lBQ0YsQ0FBQztJQUVELGlDQUFVLEdBQVYsVUFBVyxJQUFTO1FBQXBCLGlCQU9DO1FBTkEsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3BCLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDbkMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDckIsS0FBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxrQkFBa0IsRUFBdkIsQ0FBdUIsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUE7WUFDeEUsQ0FBQztRQUNGLENBQUMsQ0FBQyxDQUFBO0lBQ0gsQ0FBQztJQUVELG9DQUFhLEdBQWI7UUFBQSxpQkFzQkM7UUFyQkEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDZCxJQUFJLFlBQVksR0FBRyxVQUFVLE1BQU07Z0JBQ2xDLEtBQUssQ0FBQyxxQ0FBcUMsR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ25FLENBQUMsQ0FBQztZQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUM7Z0JBQ3hDLE1BQU0sQ0FBQztvQkFDTixHQUFHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHO29CQUNqQixHQUFHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHO29CQUNqQixLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDO2lCQUV6QixDQUFBO1lBQ0YsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FDZjtnQkFDQyxJQUFJLEVBQUUsRUFBRTtnQkFDUixRQUFRLEVBQUUsSUFBSSxDQUFDLDhCQUE4QjthQUM3QyxDQUNELENBQUE7UUFDRixDQUFDO0lBQ0YsQ0FBQztJQUVELG9DQUFhLEdBQWI7UUFDQyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQztZQUN0QixNQUFNLEVBQUU7Z0JBQ1AsR0FBRyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUc7Z0JBQ2hDLEdBQUcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHO2FBQ2hDO1lBQ0QsU0FBUyxFQUFFLEVBQUU7WUFDYixRQUFRLEVBQUUsSUFBSTtZQUNkLE9BQU8sRUFBRSxHQUFHO1lBQ1osSUFBSSxFQUFFLEVBQUU7WUFDUixRQUFRLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtTQUNqQyxDQUFDLENBQUE7SUFDSCxDQUFDO0lBRUQsZ0NBQVMsR0FBVDtRQUNDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO1lBQ3BCLEVBQUUsRUFBRSxDQUFDO1lBQ0wsS0FBSyxFQUFFLFNBQVM7WUFDaEIsS0FBSyxFQUFFLENBQUM7WUFDUixNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLO1NBQ3ZCLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCx5Q0FBa0IsR0FBbEI7UUFBQSxpQkFnQ0M7UUEvQkEsSUFBSSxDQUFDO1lBQ0osRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDckIsR0FBRyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsZUFBZSxFQUFFLENBQUMsRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDO3FCQUNuRyxJQUFJLENBQUMsVUFBQSxlQUFlO29CQUNwQixJQUFJLFlBQVksR0FBRzt3QkFDbEIsUUFBUSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUc7d0JBQ3JDLFNBQVMsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHO3dCQUN0QyxRQUFRLEVBQUUsR0FBRzt3QkFDYixrQkFBa0IsRUFBRSxFQUFFO3dCQUN0QixnQkFBZ0IsRUFBRSxFQUFFO3dCQUNwQixLQUFLLEVBQUUsQ0FBQzt3QkFDUixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7d0JBQ3JCLE9BQU8sRUFBRSxJQUFJO3dCQUNiLEdBQUcsRUFBRSxJQUFJO3FCQUNULENBQUM7b0JBQ0YsSUFBSSxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsQ0FBQztvQkFDbkUsa0VBQWtFO29CQUNsRSxLQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsR0FBRyxDQUFDLGdCQUFnQixHQUFHLElBQUk7d0JBQ2xELENBQUUsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOzhCQUNwQyxHQUFHLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzswQkFDdkUsR0FBRyxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQSxLQUFLO29CQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2pELENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQztRQUNGLENBQUM7UUFDRCxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ1YsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUMsQ0FBQztJQUNGLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQy9CO1lBQ0M7Z0JBQ0MsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDcEMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxHQUFHLFdBQVcsR0FBRyxRQUFRO2dCQUN4RCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsbURBQW1ELEdBQUcsaURBQWlEO2dCQUN6SSxrQkFBa0IsRUFBRSxLQUFLO2FBQ3pCO1lBQ0QsSUFBSTtZQUNKLGlEQUFpRDtZQUNqRCw2REFBNkQ7WUFDN0QsMkhBQTJIO1lBQzNILDZCQUE2QjtZQUM3QixLQUFLO1lBQ0w7Z0JBQ0MsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQztnQkFDeEMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxHQUFHLFdBQVcsR0FBRyxRQUFRO2dCQUN4RCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsdURBQXVELEdBQUcsMkNBQTJDO2dCQUN2SSxrQkFBa0IsRUFBRSxLQUFLO2FBQ3pCO1NBQ0QsQ0FBQyxDQUFDO1FBRUosSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNsQyw2QkFBNkI7SUFDOUIsQ0FBQztJQUVELGdDQUFTLEdBQVQ7UUFBQSxpQkFlQztRQWRBLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFDLEdBQUc7Z0JBQzVELEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDNUQsQ0FBQyxDQUFDLENBQUE7UUFDSCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMzQixDQUFDO0lBQ0YsQ0FBQztJQUVELGtDQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUNGLG1CQUFDO0FBQUQsQ0FBQyxBQTdORCxJQTZOQztBQTdOWSxZQUFZO0lBSnhCLGdCQUFTLENBQUM7UUFDVixRQUFRLEVBQUUsUUFBUTtRQUNsQixXQUFXLEVBQUUsc0JBQXNCO0tBQ25DLENBQUM7cUNBbUJ5QixXQUFJLEVBQWdCLDBCQUFXLEVBQW9CLDhDQUFrQjtHQWxCbkYsWUFBWSxDQTZOeEI7QUE3Tlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBQYXJhbGxheFZpZXcgfSBmcm9tICduYXRpdmVzY3JpcHQtbmcyLXBhcmFsbGF4L25hdGl2ZXNjcmlwdC1uZzItcGFyYWxsYXgnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgeyBCdXR0b24gfSBmcm9tIFwidWkvYnV0dG9uXCI7XG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZGF0YS5zZXJ2aWNlJztcbmltcG9ydCAqIGFzIGdlbyBmcm9tICduYXRpdmVzY3JpcHQtZ2VvbG9jYXRpb24nO1xuaW1wb3J0IHsgTlNNYXRlcmlhbFNob3djYXNlVmlldyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1tYXRlcmlhbC1zaG93Y2FzZXZpZXcnO1xuaW1wb3J0ICogYXMgZWxlbWVudFJlZ2lzdHJ5TW9kdWxlIGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnknO1xuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSAnbmF0aXZlc2NyaXB0LXRvYXN0JztcbmltcG9ydCAqIGFzIHBsYXRmb3JtIGZyb20gJ3BsYXRmb3JtJztcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIGZzIGZyb20gJ2ZpbGUtc3lzdGVtJztcbmltcG9ydCB7IFROU1BsYXllciB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hdWRpbyc7XG5pbXBvcnQgeyBUTlNGb250SWNvblNlcnZpY2UgfSBmcm9tICduYXRpdmVzY3JpcHQtbmcyLWZvbnRpY29uJztcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gJ2FwcGxpY2F0aW9uJztcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInVpL2RpYWxvZ3NcIjtcblxuXG5lbGVtZW50UmVnaXN0cnlNb2R1bGUucmVnaXN0ZXJFbGVtZW50KFwiQ2FyZFZpZXdcIiwgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1jYXJkdmlld1wiKS5DYXJkVmlldyk7XG5lbGVtZW50UmVnaXN0cnlNb2R1bGUucmVnaXN0ZXJFbGVtZW50KFwiR2lmXCIsICgpID0+IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtZ2lmXCIpLkdpZik7XG5cblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiBcIm15LWFwcFwiLFxuXHR0ZW1wbGF0ZVVybDogJy4vYXBwLmNvbXBvbmVudC5odG1sJyxcbn0pXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0bGFuZzogc3RyaW5nID0gJ3VrJztcblx0Y3VycmVudEl0ZW06IGFueTtcblx0bWFwOiBhbnk7XG5cdGdlb1dhdGNoZXI6IGFueSA9IG51bGw7XG5cdGRpc3RhbmNlVGVtcGxhdGU6IGFueTtcblx0ZGlzdGFuY2VUb0l0ZW06IHN0cmluZztcblx0cHJpdmF0ZSBzaG93Y2FzZVZpZXc6IE5TTWF0ZXJpYWxTaG93Y2FzZVZpZXc7XG5cdG1hcEhlaWdodDogbnVtYmVyID0gcGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4uaGVpZ2h0UGl4ZWxzID49IDEyODAgPyA0MDAgOiBwbGF0Zm9ybS5zY3JlZW4ubWFpblNjcmVlbi5oZWlnaHRQaXhlbHMgKiAwLjMzO1xuXHRkYXRhTG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG5cdGl0ZW1JbWFnZUxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuXHRjdXJyZW50SXRlbUltYWdlOiBzdHJpbmc7XG5cdGl0ZW1zU3Vic2NyaXB0aW9uOiBhbnk7XG5cdGF1ZGlvV29ya2VyID0gbmV3IFdvcmtlcignLi93b3JrZXJzL2F1ZGlvLXBsYXllcicpO1xuXHRhdWRpb0lzUGxheWluZzogYm9vbGVhbiA9IGZhbHNlO1xuXHRhdWRpb0lzUGF1c2VkOiBib29sZWFuID0gZmFsc2U7XG5cdGF1ZGlvUHJvZ3Jlc3NWYWx1ZTogbnVtYmVyID0gMDtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgZGF0YTogRGF0YVNlcnZpY2UsIHByaXZhdGUgZm9udGljb246IFROU0ZvbnRJY29uU2VydmljZSkge1xuXHRcdHBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XHR0aGlzLmRpc3RhbmNlVGVtcGxhdGUgPSB0aGlzLmRhdGEuZGlzdGFuY2VUZW1wbGF0ZTtcblx0XHR0aGlzLmRpc3RhbmNlVG9JdGVtID0gdGhpcy5kaXN0YW5jZVRlbXBsYXRlLmRpc3RhbmNlVG9JdGVtW3RoaXMubGFuZ107XG5cdFx0dGhpcy5kYXRhLmxvYWREYXRhKChpc0RhdGFMb2FkZWQpID0+IHsgdGhpcy5kYXRhTG9hZGVkID0gaXNEYXRhTG9hZGVkOyB9KTtcblx0XHR0aGlzLnNob3djYXNlVmlldyA9IG5ldyBOU01hdGVyaWFsU2hvd2Nhc2VWaWV3KCk7XG5cdFx0dGhpcy5kYXRhLml0ZW1zU3ViamVjdC5zdWJzY3JpYmUoKHJlcykgPT4ge1xuXHRcdFx0dGhpcy5jdXJyZW50SXRlbSA9IHRoaXMuZGF0YS5nZXRGaXJzdEl0ZW0oKTtcblx0XHRcdHRoaXMuaGFuZGxlSXRlbUltYWdlKCk7XG5cdFx0XHR0aGlzLmRyYXdPbk1hcEluaXQoKTtcblx0XHR9KTtcblx0XHRhcHBsaWNhdGlvbi5vbihhcHBsaWNhdGlvbi5zdXNwZW5kRXZlbnQsIChhcmdzOiBhcHBsaWNhdGlvbi5BcHBsaWNhdGlvbkV2ZW50RGF0YSkgPT4ge1xuXHRcdFx0dGhpcy5yZXNldFBsYXllcigpO1xuXHRcdH0pO1xuXHRcdHRoaXMuYXVkaW9Xb3JrZXIub25tZXNzYWdlID0gKG1zZykgPT4ge1xuXHRcdFx0aWYgKCdjdXJyZW50UHJvZ3Jlc3MnIGluIG1zZy5kYXRhKSB7XG5cdFx0XHRcdHRoaXMuYXVkaW9Qcm9ncmVzc1ZhbHVlID0gbXNnLmRhdGEuY3VycmVudFByb2dyZXNzO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdG5nT25Jbml0KCkge1xuXHRcdGxldCBvcHRpb25zID0ge1xuXHRcdFx0dGl0bGU6IFwiU2VsZWN0IGxhbmd1YWdlXCIsXG5cdFx0XHRtZXNzYWdlOiBcIkNob29zZSB5b3VyIGxhbmd1YWdlXCIsXG5cdFx0XHQvLyBjYW5jZWxCdXR0b25UZXh0OiBcIkNhbmNlbFwiLFxuXHRcdFx0YWN0aW9uczogW1wiRW5nbGlzaFwiLCBcIlVrcmFpbmlhblwiXVxuXHRcdH07XG5cdFx0ZGlhbG9ncy5hY3Rpb24ob3B0aW9ucykudGhlbigocmVzdWx0OiBzdHJpbmcpID0+IHtcblx0XHRcdHRoaXMubGFuZyA9IHJlc3VsdCA9PT0gJ1VrcmFpbmlhbicgPyAndWsnIDogJ2VuJztcblx0XHRcdHRoaXMuc2hvd0hlbHAoKTtcblx0XHR9KTtcblx0fVxuXG5cdHByZXZJdGVtKCkge1xuXHRcdHRoaXMuY2hhbmdlSXRlbSh0aGlzLmRhdGEuZ2V0UHJldkl0ZW0oKSk7XG5cdH1cblxuXHRuZXh0SXRlbSgpIHtcblx0XHR0aGlzLmNoYW5nZUl0ZW0odGhpcy5kYXRhLmdldE5leHRJdGVtKCkpO1xuXHR9XG5cblx0Y2hhbmdlSXRlbShuZXdJdGVtOiBhbnkpIHtcblx0XHR0aGlzLml0ZW1JbWFnZUxvYWRlZCA9IGZhbHNlO1xuXHRcdHRoaXMuY3VycmVudEl0ZW0gPSBuZXdJdGVtO1xuXHRcdHRoaXMuaGFuZGxlSXRlbUltYWdlKCk7XG5cdFx0dGhpcy5yZXNldFBsYXllcigpO1xuXHRcdHRoaXMuc2hvd0Rpc3RhbmNlVG9JdGVtKCk7XG5cdFx0dGhpcy5hbmltYXRlVG9JdGVtKCk7XG5cdH1cblxuXHRoYW5kbGVJdGVtSW1hZ2UoKSB7XG5cdFx0aWYgKHRoaXMuZGF0YS5maWxlRXhpc3RzKHRoaXMuY3VycmVudEl0ZW0uaW1hZ2UpKSB7XG5cdFx0XHR0aGlzLmN1cnJlbnRJdGVtSW1hZ2UgPSB0aGlzLmRhdGEuZ2V0RnVsbEZpbGVQYXRoKHRoaXMuY3VycmVudEl0ZW0uaW1hZ2UpO1xuXHRcdFx0dGhpcy5pdGVtSW1hZ2VMb2FkZWQgPSB0cnVlO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLmRhdGEuZG93bmxvYWRGaWxlKHRoaXMuY3VycmVudEl0ZW0uaW1hZ2UsIChmaWxlKSA9PiB7XG5cdFx0XHRcdHRoaXMuY3VycmVudEl0ZW1JbWFnZSA9IGZpbGU7XG5cdFx0XHRcdHRoaXMuaXRlbUltYWdlTG9hZGVkID0gdHJ1ZTtcblx0XHRcdH0pO1xuXHRcdH1cblx0fVxuXG5cdG9uTWFwUmVhZHkoYXJnczogYW55KSB7XG5cdFx0dGhpcy5tYXAgPSBhcmdzLm1hcDtcblx0XHRnZW8uZW5hYmxlTG9jYXRpb25SZXF1ZXN0KCkudGhlbihyZXMgPT4ge1xuXHRcdFx0aWYgKGdlby5pc0VuYWJsZWQoKSkge1xuXHRcdFx0XHR0aGlzLmdlb1dhdGNoZXIgPSBzZXRJbnRlcnZhbCgoKSA9PiB0aGlzLnNob3dEaXN0YW5jZVRvSXRlbSwgNjAgKiAxMDAwKVxuXHRcdFx0fVxuXHRcdH0pXG5cdH1cblxuXHRkcmF3T25NYXBJbml0KCkge1xuXHRcdGlmICh0aGlzLm1hcCkge1xuXHRcdFx0dmFyIG9uQ2FsbG91dFRhcCA9IGZ1bmN0aW9uIChtYXJrZXIpIHtcblx0XHRcdFx0YWxlcnQoXCJNYXJrZXIgY2FsbG91dCB0YXBwZWQgd2l0aCB0aXRsZTogJ1wiICsgbWFya2VyLnRpdGxlICsgXCInXCIpO1xuXHRcdFx0fTtcblx0XHRcdHRoaXMubWFwLmFkZE1hcmtlcnModGhpcy5kYXRhLml0ZW1zLm1hcCh2ID0+IHtcblx0XHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0XHRsYXQ6IHYuY29vcmRzLmxhdCxcblx0XHRcdFx0XHRsbmc6IHYuY29vcmRzLmxuZyxcblx0XHRcdFx0XHR0aXRsZTogdi50aXRsZVt0aGlzLmxhbmddLFxuXHRcdFx0XHRcdC8vIG9uQ2FsbG91dFRhcDogb25DYWxsb3V0VGFwXG5cdFx0XHRcdH1cblx0XHRcdH0pKTtcblx0XHRcdHRoaXMuc2hvd0Rpc3RhbmNlVG9JdGVtKCk7XG5cdFx0XHR0aGlzLmRyYXdSb3V0ZSgpO1xuXHRcdFx0dGhpcy5tYXAuc2V0VGlsdChcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHRpbHQ6IDkwLCAvLyBkZWZhdWx0IDMwIChkZWdyZWVzIGFuZ2xlKVxuXHRcdFx0XHRcdGR1cmF0aW9uOiAzMDAwIC8vIGRlZmF1bHQgNTAwMCAobWlsbGlzZWNvbmRzKVxuXHRcdFx0XHR9XG5cdFx0XHQpXG5cdFx0fVxuXHR9XG5cblx0YW5pbWF0ZVRvSXRlbSgpIHtcblx0XHR0aGlzLm1hcC5hbmltYXRlQ2FtZXJhKHtcblx0XHRcdHRhcmdldDoge1xuXHRcdFx0XHRsYXQ6IHRoaXMuY3VycmVudEl0ZW0uY29vcmRzLmxhdCxcblx0XHRcdFx0bG5nOiB0aGlzLmN1cnJlbnRJdGVtLmNvb3Jkcy5sbmcsXG5cdFx0XHR9LFxuXHRcdFx0em9vbUxldmVsOiAxNywgLy8gQW5kcm9pZFxuXHRcdFx0YWx0aXR1ZGU6IDIwMDAsIC8vIGlPUyAobWV0ZXJzIGZyb20gdGhlIGdyb3VuZClcblx0XHRcdGJlYXJpbmc6IDI3MCwgLy8gV2hlcmUgdGhlIGNhbWVyYSBpcyBwb2ludGluZywgMC0zNjAgKGRlZ3JlZXMpXG5cdFx0XHR0aWx0OiA1MCxcblx0XHRcdGR1cmF0aW9uOiAxMDAwIC8vIGluIG1pbGxpc2Vjb25kc1xuXHRcdH0pXG5cdH1cblxuXHRkcmF3Um91dGUoKSB7XG5cdFx0dGhpcy5tYXAuYWRkUG9seWxpbmUoe1xuXHRcdFx0aWQ6IDEsIC8vIG9wdGlvbmFsLCBjYW4gYmUgdXNlZCBpbiAncmVtb3ZlUG9seWxpbmVzJ1xuXHRcdFx0Y29sb3I6ICcjMzM2Njk5JywgLy8gU2V0IHRoZSBjb2xvciBvZiB0aGUgbGluZSAoZGVmYXVsdCBibGFjaylcblx0XHRcdHdpZHRoOiA3LCAvL1NldCB0aGUgd2lkdGggb2YgdGhlIGxpbmUgKGRlZmF1bHQgNSlcblx0XHRcdHBvaW50czogdGhpcy5kYXRhLnJvdXRlXG5cdFx0fSk7XG5cdH1cblxuXHRzaG93RGlzdGFuY2VUb0l0ZW0oKSB7XG5cdFx0dHJ5IHtcblx0XHRcdGlmIChnZW8uaXNFbmFibGVkKCkpIHtcblx0XHRcdFx0Z2VvLmdldEN1cnJlbnRMb2NhdGlvbih7IGRlc2lyZWRBY2N1cmFjeTogMywgdXBkYXRlRGlzdGFuY2U6IDEwLCBtYXhpbXVtQWdlOiAyMDAwMCwgdGltZW91dDogMjAwMDAgfSlcblx0XHRcdFx0XHQudGhlbihjdXJyZW50TG9jYXRpb24gPT4ge1xuXHRcdFx0XHRcdFx0bGV0IGl0ZW1Mb2NhdGlvbiA9IHtcblx0XHRcdFx0XHRcdFx0bGF0aXR1ZGU6IHRoaXMuY3VycmVudEl0ZW0uY29vcmRzLmxhdCxcblx0XHRcdFx0XHRcdFx0bG9uZ2l0dWRlOiB0aGlzLmN1cnJlbnRJdGVtLmNvb3Jkcy5sbmcsXG5cdFx0XHRcdFx0XHRcdGFsdGl0dWRlOiAxODEsXG5cdFx0XHRcdFx0XHRcdGhvcml6b250YWxBY2N1cmFjeTogMTAsXG5cdFx0XHRcdFx0XHRcdHZlcnRpY2FsQWNjdXJhY3k6IDEwLFxuXHRcdFx0XHRcdFx0XHRzcGVlZDogMixcblx0XHRcdFx0XHRcdFx0ZGlyZWN0aW9uOiAwLFxuXHRcdFx0XHRcdFx0XHR0aW1lc3RhbXA6IG5ldyBEYXRlKCksXG5cdFx0XHRcdFx0XHRcdGFuZHJvaWQ6IG51bGwsXG5cdFx0XHRcdFx0XHRcdGlvczogbnVsbFxuXHRcdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRcdGxldCBkaXN0YW5jZUluTWV0ZXJzID0gZ2VvLmRpc3RhbmNlKGN1cnJlbnRMb2NhdGlvbiwgaXRlbUxvY2F0aW9uKTtcblx0XHRcdFx0XHRcdC8vIFRvYXN0Lm1ha2VUZXh0KFwiZGlzdGFuY2VJbk1ldGVycyA9IFwiICtkaXN0YW5jZUluTWV0ZXJzKS5zaG93KCk7XG5cdFx0XHRcdFx0XHR0aGlzLmRpc3RhbmNlVG9JdGVtID0gXCJcIiArIChkaXN0YW5jZUluTWV0ZXJzID4gMTAwMSA/XG5cdFx0XHRcdFx0XHRcdCsgKGRpc3RhbmNlSW5NZXRlcnMgLyAxMDAwKS50b0ZpeGVkKDIpXG5cdFx0XHRcdFx0XHRcdCsgXCIgXCIgKyB0aGlzLmRpc3RhbmNlVGVtcGxhdGUua21bdGhpcy5sYW5nXSA6IGRpc3RhbmNlSW5NZXRlcnMudG9GaXhlZCgwKVxuXHRcdFx0XHRcdFx0XHQrIFwiIFwiICsgdGhpcy5kaXN0YW5jZVRlbXBsYXRlLm1bdGhpcy5sYW5nXSk7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4ge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coXCJnZXRDdXJyZW50TG9jYXRpb24gZXJyb3I6XCIsIGVycm9yKTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0fVxuXHRcdH1cblx0XHRjYXRjaCAoZSkge1xuXHRcdFx0VG9hc3QubWFrZVRleHQoSlNPTi5zdHJpbmdpZnkoZSkpLnNob3coKTtcblx0XHR9XG5cdH1cblxuXHRzaG93SGVscCgpIHtcblx0XHR0aGlzLnNob3djYXNlVmlldy5jcmVhdGVTZXF1ZW5jZShcblx0XHRcdFtcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHRhcmdldDogdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwibWFwXCIpLFxuXHRcdFx0XHRcdGRpc21pc3NUZXh0OiB0aGlzLmxhbmcgPT09IFwidWtcIiA/IFwi0JfRgNC+0LfRg9C80ZbQu9C+XCIgOiBcIkdPVCBJVFwiLFxuXHRcdFx0XHRcdGNvbnRlbnRUZXh0OiB0aGlzLmxhbmcgPT09IFwidWtcIiA/IFwi0KbQtSDQutCw0YDRgtCwLCDQtNC1INC/0L7QutCw0LfQsNC90ZYg0YLRg9GA0LjRgdGC0LjRh9C90ZYg0L7QsSfRlNC60YLQuCDQvNCw0YDRiNGA0YPRgtGDXCIgOiBcIlRoaXMgaXMgbWFwLCB3aGVyZSB0b3VyaXN0IGl0ZW1zIHdpbGwgYmUgc2hvd24uXCIsXG5cdFx0XHRcdFx0d2l0aFJlY3RhbmdsZVNoYXBlOiBmYWxzZVxuXHRcdFx0XHR9LFxuXHRcdFx0XHQvLyB7XG5cdFx0XHRcdC8vIFx0dGFyZ2V0OiB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJwbGF5X2J1dHRvblwiKSxcblx0XHRcdFx0Ly8gXHRkaXNtaXNzVGV4dDogdGhpcy5sYW5nID09PSBcInVrXCIgPyBcItCX0YDQvtC30YPQvNGW0LvQvlwiIDogXCJHT1QgSVRcIixcblx0XHRcdFx0Ly8gXHRjb250ZW50VGV4dDogdGhpcy5sYW5nID09PSBcInVrXCIgPyBcItCm0Y8g0LrQvdC+0L/QutCwINC00L7Qt9Cy0L7Qu9GP0ZQg0L/RgNC+0YHQu9GD0YXQsNGC0Lgg0LDRg9C00ZbQvtCz0ZbQtC5cIiA6IFwiVGhpcyBidXR0b24gbGV0IHlvdSBsaXN0ZW4gYXVkaW9ndWlkZS5cIixcblx0XHRcdFx0Ly8gXHR3aXRoUmVjdGFuZ2xlU2hhcGU6IGZhbHNlXG5cdFx0XHRcdC8vIH0sXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR0YXJnZXQ6IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImNvbnRlbnRcIiksXG5cdFx0XHRcdFx0ZGlzbWlzc1RleHQ6IHRoaXMubGFuZyA9PT0gXCJ1a1wiID8gXCLQl9GA0L7Qt9GD0LzRltC70L5cIiA6IFwiR09UIElUXCIsXG5cdFx0XHRcdFx0Y29udGVudFRleHQ6IHRoaXMubGFuZyA9PT0gXCJ1a1wiID8gXCLQotGD0YIg0LLRltC00L7QsdGA0LDQttCw0ZTRgtGM0YHRjyDRltC90YTQvtGA0LzQsNGG0ZbRjyDQv9GA0L4g0YLRg9GA0LjRgdGC0LjRh9C90LjQuSDQvtCxJ9GU0LrRgi5cIiA6IFwiSGVyZSB5b3UgY2FuIHNlZSBpbmZvIGFib3V0IHRvdXJpc3QgaXRlbS5cIixcblx0XHRcdFx0XHR3aXRoUmVjdGFuZ2xlU2hhcGU6IGZhbHNlXG5cdFx0XHRcdH0sXG5cdFx0XHRdKTtcblxuXHRcdHRoaXMuc2hvd2Nhc2VWaWV3LnN0YXJ0U2VxdWVuY2UoKTtcblx0XHQvLyB0aGlzLnNob3djYXNlVmlldy5yZXNldCgpO1xuXHR9XG5cblx0cGxheVBhdXNlKCkge1xuXHRcdGlmICghdGhpcy5hdWRpb0lzUGxheWluZyAmJiAhdGhpcy5hdWRpb0lzUGF1c2VkKSB7XG5cdFx0XHR0aGlzLmRhdGEuZ2V0QXVkaW9VcmwodGhpcy5jdXJyZW50SXRlbS5hdWRpb1t0aGlzLmxhbmddLCAodXJsKSA9PiB7XG5cdFx0XHRcdHRoaXMuYXVkaW9Jc1BsYXlpbmcgPSB0cnVlO1xuXHRcdFx0XHR0aGlzLmF1ZGlvV29ya2VyLnBvc3RNZXNzYWdlKHsgdXJsOiB1cmwsIGFjdGlvbjogXCJwbGF5XCIgfSk7XG5cdFx0XHR9KVxuXHRcdH0gZWxzZSBpZiAoIXRoaXMuYXVkaW9Jc1BsYXlpbmcgJiYgdGhpcy5hdWRpb0lzUGF1c2VkKSB7XG5cdFx0XHR0aGlzLmF1ZGlvV29ya2VyLnBvc3RNZXNzYWdlKHsgYWN0aW9uOiBcInJlc3VtZVwiIH0pO1xuXHRcdFx0dGhpcy5hdWRpb0lzUGxheWluZyA9IHRydWU7XG5cdFx0XHR0aGlzLmF1ZGlvSXNQYXVzZWQgPSBmYWxzZTtcblx0XHR9IGVsc2UgaWYgKHRoaXMuYXVkaW9Jc1BsYXlpbmcpIHtcblx0XHRcdHRoaXMuYXVkaW9Xb3JrZXIucG9zdE1lc3NhZ2UoeyBhY3Rpb246IFwicGF1c2VcIiB9KTtcblx0XHRcdHRoaXMuYXVkaW9Jc1BsYXlpbmcgPSBmYWxzZTtcblx0XHRcdHRoaXMuYXVkaW9Jc1BhdXNlZCA9IHRydWU7XG5cdFx0fVxuXHR9XG5cblx0cmVzZXRQbGF5ZXIoKSB7XG5cdFx0dGhpcy5hdWRpb0lzUGxheWluZyA9IGZhbHNlO1xuXHRcdHRoaXMuYXVkaW9Jc1BhdXNlZCA9IGZhbHNlO1xuXHRcdHRoaXMuYXVkaW9Qcm9ncmVzc1ZhbHVlID0gMDtcblx0XHR0aGlzLmF1ZGlvV29ya2VyLnBvc3RNZXNzYWdlKHsgYWN0aW9uOiBcInJlc2V0XCIgfSk7XG5cdH1cbn1cblxuIl19