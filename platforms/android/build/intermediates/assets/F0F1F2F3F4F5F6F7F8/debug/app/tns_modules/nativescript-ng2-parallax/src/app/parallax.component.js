"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var _scrollViewModule = require('ui/scroll-view');
var _frame = require('ui/frame');
var ParallaxView = (function () {
    function ParallaxView() {
        this.page = _frame.topmost().currentPage;
        this.viewsToFade = [];
        if (this.topViewHeight == null) {
            this.topViewHeight = 300; //default height if it is not set.
        }
        if (this.controlsToFade == null) {
            this.controlsToFade = [];
        }
    }
    ParallaxView.prototype.ngOnInit = function () {
        var _this = this;
        var prevOffset = -10;
        var topOpacity = 1;
        this.topView = this.page.getViewById('topView');
        this.topView.height = this.topViewHeight;
        //find each control specified to fade.
        this.controlsToFade.forEach(function (id) {
            var newView = _this.page.getViewById(id);
            if (newView != null) {
                _this.viewsToFade.push(newView);
            }
        });
        this.scrollView = this.page.getViewById('scrollView');
        this.scrollView.on(_scrollViewModule.ScrollView.scrollEvent, function (args) {
            if (prevOffset <= _this.scrollView.verticalOffset) {
                if (_this.topView.height >= 0) {
                    _this.topView.height = _this.getTopViewHeight(_this.topViewHeight, _this.scrollView.verticalOffset);
                }
            }
            else {
                if (_this.topView.height <= _this.topViewHeight) {
                    _this.topView.height = _this.getTopViewHeight(_this.topViewHeight, _this.scrollView.verticalOffset);
                }
            }
            //fades in and out label in topView
            if (_this.scrollView.verticalOffset < _this.topViewHeight) {
                topOpacity = parseFloat((1 - (_this.scrollView.verticalOffset * 0.01)).toString());
                if (topOpacity > 0 && topOpacity <= 1) {
                    //fade each control
                    _this.viewsToFade.forEach(function (view) {
                        view.opacity = topOpacity;
                    });
                }
            }
            prevOffset = _this.scrollView.verticalOffset;
        });
    };
    ParallaxView.prototype.getTopViewHeight = function (topHeight, offset) {
        if ((topHeight - offset) >= 0) {
            return topHeight - offset;
        }
        else {
            return 0;
        }
    };
    __decorate([
        core_1.Input('head-height'), 
        __metadata('design:type', Number)
    ], ParallaxView.prototype, "topViewHeight", void 0);
    __decorate([
        core_1.Input('controls-to-fade'), 
        __metadata('design:type', Array)
    ], ParallaxView.prototype, "controlsToFade", void 0);
    ParallaxView = __decorate([
        core_1.Component({
            selector: 'parallax-view',
            template: "\n\t\t\t<ScrollView id=\"scrollView\">\n\t\t\t\t<StackLayout id=\"scrollViewContent\">\n\t\t\t\t\t<StackLayout id=\"topView\" class=\"top-content\">\n\t\t\t\t\t\t<ng-content select=\"[head]\"></ng-content>\n\t\t\t\t\t</StackLayout>\n\t\t\t\t\t<StackLayout id=\"bottomView\">\n\t\t\t\t\t\t<ng-content select=\"[body]\"></ng-content>\n\t\t\t\t\t</StackLayout>\n\t\t\t\t</StackLayout>\n\t\t\t</ScrollView>\n\t",
            styles: ["\n\t"]
        }), 
        __metadata('design:paramtypes', [])
    ], ParallaxView);
    return ParallaxView;
}());
exports.ParallaxView = ParallaxView;
//# sourceMappingURL=parallax.component.js.map