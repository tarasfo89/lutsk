import { OnInit } from '@angular/core';
export declare class ParallaxView implements OnInit {
    private page;
    private topView;
    private scrollView;
    private viewsToFade;
    topViewHeight: number;
    controlsToFade: string[];
    constructor();
    ngOnInit(): void;
    getTopViewHeight(topHeight: number, offset: number): number;
}
