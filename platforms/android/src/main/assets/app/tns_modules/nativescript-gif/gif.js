"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Common = require("./gif.common");
var fs = require("file-system");
var http = require("http");
global.moduleMerge(Common, exports);
function onSrcPropertyChanged(data) {
    var gif = data.object;
    if (!gif.android) {
        return;
    }
    gif.src = data.newValue;
}
Common.Gif.srcProperty.metadata.onSetNativeValue = onSrcPropertyChanged;
var Gif = (function (_super) {
    __extends(Gif, _super);
    function Gif() {
        return _super.call(this) || this;
    }
    Object.defineProperty(Gif.prototype, "android", {
        get: function () {
            return this._android;
        },
        enumerable: true,
        configurable: true
    });
    Gif.prototype._createUI = function () {
        this._android = new pl.droidsonroids.gif.GifImageView(this._context);
        var _this = this;
        if (this.src) {
            var isUrl = false;
            if (this.src.indexOf("://") !== -1) {
                if (this.src.indexOf('res://') === -1) {
                    isUrl = true;
                }
            }
            if (!isUrl) {
                var currentPath = fs.knownFolders.currentApp().path;
                if (this.src[1] === '/' && (this.src[0] === '.' || this.src[0] === '~')) {
                    this.src = this.src.substr(2);
                }
                if (this.src[0] !== '/') {
                    this.src = currentPath + '/' + this.src;
                }
                this._drawable = new pl.droidsonroids.gif.GifDrawable(this.src);
                this._android.setImageDrawable(this._drawable);
            }
            else {
                http.request({ url: this.src, method: "GET" }).then(function (r) {
                    _this._drawable = new pl.droidsonroids.gif.GifDrawable(r.content.raw.toByteArray());
                    _this._android.setImageDrawable(_this._drawable);
                    console.log('this._drawable: ' + this._drawable);
                }, function (err) {
                    console.log(err);
                });
            }
        }
        else {
            console.log("No src property set for the Gif");
        }
    };
    Gif.prototype.stop = function () {
        if (this._drawable)
            this._drawable.stop();
    };
    Gif.prototype.start = function () {
        if (this._drawable)
            this._drawable.start();
    };
    Gif.prototype.isPlaying = function () {
        if (this._drawable) {
            var isPlaying = this._drawable.isRunning();
            return isPlaying;
        }
        else {
            return false;
        }
    };
    Gif.prototype.getFrameCount = function () {
        if (this._drawable)
            var frames = this._drawable.getNumberOfFrames();
        return frames;
    };
    Gif.prototype.reset = function () {
        this._drawable.reset();
    };
    Gif.prototype.getDuration = function () {
        if (this._drawable) {
            var duration = this._drawable.getDuration();
            return duration;
        }
        else {
            return 0;
        }
    };
    Gif.prototype.setSpeed = function (factor) {
        if (this._drawable)
            this._drawable.setSpeed(factor);
    };
    Gif.prototype.recycle = function () {
        if (this._drawable)
            this._drawable.recycle();
    };
    return Gif;
}(Common.Gif));
exports.Gif = Gif;
//# sourceMappingURL=gif.js.map