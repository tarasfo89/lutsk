"use strict";
var color_1 = require("color");
var Colors = (function () {
    function Colors() {
    }
    Object.defineProperty(Colors.prototype, "md_red_50", {
        get: function () { return new color_1.Color("#FFEBEE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_100", {
        get: function () { return new color_1.Color("#FFCDD2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_200", {
        get: function () { return new color_1.Color("#EF9A9A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_300", {
        get: function () { return new color_1.Color("#E57373").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_400", {
        get: function () { return new color_1.Color("#EF5350").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_500", {
        get: function () { return new color_1.Color("#F44336").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_600", {
        get: function () { return new color_1.Color("#E53935").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_700", {
        get: function () { return new color_1.Color("#D32F2F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_800", {
        get: function () { return new color_1.Color("#C62828").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_900", {
        get: function () { return new color_1.Color("#B71C1C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_A100", {
        get: function () { return new color_1.Color("#FF8A80").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_A200", {
        get: function () { return new color_1.Color("#FF5252").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_A400", {
        get: function () { return new color_1.Color("#FF1744").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_red_A700", {
        get: function () { return new color_1.Color("#D50000").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_50", {
        get: function () { return new color_1.Color("#FCE4EC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_100", {
        get: function () { return new color_1.Color("#F8BBD0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_200", {
        get: function () { return new color_1.Color("#F48FB1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_300", {
        get: function () { return new color_1.Color("#F06292").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_400", {
        get: function () { return new color_1.Color("#EC407A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_500", {
        get: function () { return new color_1.Color("#E91E63").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_600", {
        get: function () { return new color_1.Color("#D81B60").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_700", {
        get: function () { return new color_1.Color("#C2185B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_800", {
        get: function () { return new color_1.Color("#AD1457").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_900", {
        get: function () { return new color_1.Color("#880E4F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_A100", {
        get: function () { return new color_1.Color("#FF80AB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_A200", {
        get: function () { return new color_1.Color("#FF4081").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_A400", {
        get: function () { return new color_1.Color("#F50057").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_pink_A700", {
        get: function () { return new color_1.Color("#C51162").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_50", {
        get: function () { return new color_1.Color("#F3E5F5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_100", {
        get: function () { return new color_1.Color("#E1BEE7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_200", {
        get: function () { return new color_1.Color("#CE93D8").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_300", {
        get: function () { return new color_1.Color("#BA68C8").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_400", {
        get: function () { return new color_1.Color("#AB47BC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_500", {
        get: function () { return new color_1.Color("#9C27B0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_600", {
        get: function () { return new color_1.Color("#8E24AA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_700", {
        get: function () { return new color_1.Color("#7B1FA2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_800", {
        get: function () { return new color_1.Color("#6A1B9A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_900", {
        get: function () { return new color_1.Color("#4A148C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_A100", {
        get: function () { return new color_1.Color("#EA80FC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_A200", {
        get: function () { return new color_1.Color("#E040FB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_A400", {
        get: function () { return new color_1.Color("#D500F9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_purple_A700", {
        get: function () { return new color_1.Color("#AA00FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_50", {
        get: function () { return new color_1.Color("#EDE7F6").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_100", {
        get: function () { return new color_1.Color("#D1C4E9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_200", {
        get: function () { return new color_1.Color("#B39DDB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_300", {
        get: function () { return new color_1.Color("#9575CD").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_400", {
        get: function () { return new color_1.Color("#7E57C2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_500", {
        get: function () { return new color_1.Color("#673AB7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_600", {
        get: function () { return new color_1.Color("#5E35B1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_700", {
        get: function () { return new color_1.Color("#512DA8").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_800", {
        get: function () { return new color_1.Color("#4527A0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_900", {
        get: function () { return new color_1.Color("#311B92").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_A100", {
        get: function () { return new color_1.Color("#B388FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_A200", {
        get: function () { return new color_1.Color("#7C4DFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_A400", {
        get: function () { return new color_1.Color("#651FFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_purple_A700", {
        get: function () { return new color_1.Color("#6200EA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_50", {
        get: function () { return new color_1.Color("#E8EAF6").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_100", {
        get: function () { return new color_1.Color("#C5CAE9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_200", {
        get: function () { return new color_1.Color("#9FA8DA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_300", {
        get: function () { return new color_1.Color("#7986CB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_400", {
        get: function () { return new color_1.Color("#5C6BC0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_500", {
        get: function () { return new color_1.Color("#3F51B5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_600", {
        get: function () { return new color_1.Color("#3949AB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_700", {
        get: function () { return new color_1.Color("#303F9F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_800", {
        get: function () { return new color_1.Color("#283593").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_900", {
        get: function () { return new color_1.Color("#1A237E").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_A100", {
        get: function () { return new color_1.Color("#8C9EFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_A200", {
        get: function () { return new color_1.Color("#536DFE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_A400", {
        get: function () { return new color_1.Color("#3D5AFE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_indigo_A700", {
        get: function () { return new color_1.Color("#304FFE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_50", {
        get: function () { return new color_1.Color("#E3F2FD").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_100", {
        get: function () { return new color_1.Color("#BBDEFB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_200", {
        get: function () { return new color_1.Color("#90CAF9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_300", {
        get: function () { return new color_1.Color("#64B5F6").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_400", {
        get: function () { return new color_1.Color("#42A5F5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_500", {
        get: function () { return new color_1.Color("#2196F3").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_600", {
        get: function () { return new color_1.Color("#1E88E5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_700", {
        get: function () { return new color_1.Color("#1976D2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_800", {
        get: function () { return new color_1.Color("#1565C0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_900", {
        get: function () { return new color_1.Color("#0D47A1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_A100", {
        get: function () { return new color_1.Color("#82B1FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_A200", {
        get: function () { return new color_1.Color("#448AFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_A400", {
        get: function () { return new color_1.Color("#2979FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_A700", {
        get: function () { return new color_1.Color("#2962FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_50", {
        get: function () { return new color_1.Color("#E1F5FE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_100", {
        get: function () { return new color_1.Color("#B3E5FC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_200", {
        get: function () { return new color_1.Color("#81D4fA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_300", {
        get: function () { return new color_1.Color("#4fC3F7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_400", {
        get: function () { return new color_1.Color("#29B6FC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_500", {
        get: function () { return new color_1.Color("#03A9F4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_600", {
        get: function () { return new color_1.Color("#039BE5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_700", {
        get: function () { return new color_1.Color("#0288D1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_800", {
        get: function () { return new color_1.Color("#0277BD").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_900", {
        get: function () { return new color_1.Color("#01579B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_A100", {
        get: function () { return new color_1.Color("#80D8FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_A200", {
        get: function () { return new color_1.Color("#40C4FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_A400", {
        get: function () { return new color_1.Color("#00B0FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_blue_A700", {
        get: function () { return new color_1.Color("#0091EA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_50", {
        get: function () { return new color_1.Color("#E0F7FA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_100", {
        get: function () { return new color_1.Color("#B2EBF2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_200", {
        get: function () { return new color_1.Color("#80DEEA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_300", {
        get: function () { return new color_1.Color("#4DD0E1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_400", {
        get: function () { return new color_1.Color("#26C6DA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_500", {
        get: function () { return new color_1.Color("#00BCD4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_600", {
        get: function () { return new color_1.Color("#00ACC1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_700", {
        get: function () { return new color_1.Color("#0097A7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_800", {
        get: function () { return new color_1.Color("#00838F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_900", {
        get: function () { return new color_1.Color("#006064").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_A100", {
        get: function () { return new color_1.Color("#84FFFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_A200", {
        get: function () { return new color_1.Color("#18FFFF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_A400", {
        get: function () { return new color_1.Color("#00E5FF").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_cyan_A700", {
        get: function () { return new color_1.Color("#00B8D4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_50", {
        get: function () { return new color_1.Color("#E0F2F1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_100", {
        get: function () { return new color_1.Color("#B2DFDB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_200", {
        get: function () { return new color_1.Color("#80CBC4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_300", {
        get: function () { return new color_1.Color("#4DB6AC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_400", {
        get: function () { return new color_1.Color("#26A69A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_500", {
        get: function () { return new color_1.Color("#009688").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_600", {
        get: function () { return new color_1.Color("#00897B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_700", {
        get: function () { return new color_1.Color("#00796B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_800", {
        get: function () { return new color_1.Color("#00695C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_900", {
        get: function () { return new color_1.Color("#004D40").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_A100", {
        get: function () { return new color_1.Color("#A7FFEB").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_A200", {
        get: function () { return new color_1.Color("#64FFDA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_A400", {
        get: function () { return new color_1.Color("#1DE9B6").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_teal_A700", {
        get: function () { return new color_1.Color("#00BFA5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_50", {
        get: function () { return new color_1.Color("#E8F5E9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_100", {
        get: function () { return new color_1.Color("#C8E6C9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_200", {
        get: function () { return new color_1.Color("#A5D6A7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_300", {
        get: function () { return new color_1.Color("#81C784").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_400", {
        get: function () { return new color_1.Color("#66BB6A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_500", {
        get: function () { return new color_1.Color("#4CAF50").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_600", {
        get: function () { return new color_1.Color("#43A047").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_700", {
        get: function () { return new color_1.Color("#388E3C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_800", {
        get: function () { return new color_1.Color("#2E7D32").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_900", {
        get: function () { return new color_1.Color("#1B5E20").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_A100", {
        get: function () { return new color_1.Color("#B9F6CA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_A200", {
        get: function () { return new color_1.Color("#69F0AE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_A400", {
        get: function () { return new color_1.Color("#00E676").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_green_A700", {
        get: function () { return new color_1.Color("#00C853").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_50", {
        get: function () { return new color_1.Color("#F1F8E9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_100", {
        get: function () { return new color_1.Color("#DCEDC8").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_200", {
        get: function () { return new color_1.Color("#C5E1A5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_300", {
        get: function () { return new color_1.Color("#AED581").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_400", {
        get: function () { return new color_1.Color("#9CCC65").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_500", {
        get: function () { return new color_1.Color("#8BC34A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_600", {
        get: function () { return new color_1.Color("#7CB342").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_700", {
        get: function () { return new color_1.Color("#689F38").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_800", {
        get: function () { return new color_1.Color("#558B2F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_900", {
        get: function () { return new color_1.Color("#33691E").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_A100", {
        get: function () { return new color_1.Color("#CCFF90").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_A200", {
        get: function () { return new color_1.Color("#B2FF59").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_A400", {
        get: function () { return new color_1.Color("#76FF03").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_light_green_A700", {
        get: function () { return new color_1.Color("#64DD17").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_50", {
        get: function () { return new color_1.Color("#F9FBE7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_100", {
        get: function () { return new color_1.Color("#F0F4C3").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_200", {
        get: function () { return new color_1.Color("#E6EE9C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_300", {
        get: function () { return new color_1.Color("#DCE775").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_400", {
        get: function () { return new color_1.Color("#D4E157").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_500", {
        get: function () { return new color_1.Color("#CDDC39").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_600", {
        get: function () { return new color_1.Color("#C0CA33").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_700", {
        get: function () { return new color_1.Color("#A4B42B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_800", {
        get: function () { return new color_1.Color("#9E9D24").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_900", {
        get: function () { return new color_1.Color("#827717").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_A100", {
        get: function () { return new color_1.Color("#F4FF81").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_A200", {
        get: function () { return new color_1.Color("#EEFF41").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_A400", {
        get: function () { return new color_1.Color("#C6FF00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_lime_A700", {
        get: function () { return new color_1.Color("#AEEA00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_50", {
        get: function () { return new color_1.Color("#FFFDE7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_100", {
        get: function () { return new color_1.Color("#FFF9C4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_200", {
        get: function () { return new color_1.Color("#FFF590").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_300", {
        get: function () { return new color_1.Color("#FFF176").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_400", {
        get: function () { return new color_1.Color("#FFEE58").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_500", {
        get: function () { return new color_1.Color("#FFEB3B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_600", {
        get: function () { return new color_1.Color("#FDD835").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_700", {
        get: function () { return new color_1.Color("#FBC02D").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_800", {
        get: function () { return new color_1.Color("#F9A825").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_900", {
        get: function () { return new color_1.Color("#F57F17").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_A100", {
        get: function () { return new color_1.Color("#FFFF82").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_A200", {
        get: function () { return new color_1.Color("#FFFF00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_A400", {
        get: function () { return new color_1.Color("#FFEA00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_yellow_A700", {
        get: function () { return new color_1.Color("#FFD600").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_50", {
        get: function () { return new color_1.Color("#FFF8E1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_100", {
        get: function () { return new color_1.Color("#FFECB3").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_200", {
        get: function () { return new color_1.Color("#FFE082").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_300", {
        get: function () { return new color_1.Color("#FFD54F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_400", {
        get: function () { return new color_1.Color("#FFCA28").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_500", {
        get: function () { return new color_1.Color("#FFC107").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_600", {
        get: function () { return new color_1.Color("#FFB300").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_700", {
        get: function () { return new color_1.Color("#FFA000").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_800", {
        get: function () { return new color_1.Color("#FF8F00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_900", {
        get: function () { return new color_1.Color("#FF6F00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_A100", {
        get: function () { return new color_1.Color("#FFE57F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_A200", {
        get: function () { return new color_1.Color("#FFD740").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_A400", {
        get: function () { return new color_1.Color("#FFC400").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_amber_A700", {
        get: function () { return new color_1.Color("#FFAB00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_50", {
        get: function () { return new color_1.Color("#FFF3E0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_100", {
        get: function () { return new color_1.Color("#FFE0B2").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_200", {
        get: function () { return new color_1.Color("#FFCC80").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_300", {
        get: function () { return new color_1.Color("#FFB74D").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_400", {
        get: function () { return new color_1.Color("#FFA726").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_500", {
        get: function () { return new color_1.Color("#FF9800").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_600", {
        get: function () { return new color_1.Color("#FB8C00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_700", {
        get: function () { return new color_1.Color("#F57C00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_800", {
        get: function () { return new color_1.Color("#EF6C00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_900", {
        get: function () { return new color_1.Color("#E65100").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_A100", {
        get: function () { return new color_1.Color("#FFD180").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_A200", {
        get: function () { return new color_1.Color("#FFAB40").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_A400", {
        get: function () { return new color_1.Color("#FF9100").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_orange_A700", {
        get: function () { return new color_1.Color("#FF6D00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_50", {
        get: function () { return new color_1.Color("#FBE9A7").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_100", {
        get: function () { return new color_1.Color("#FFCCBC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_200", {
        get: function () { return new color_1.Color("#FFAB91").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_300", {
        get: function () { return new color_1.Color("#FF8A65").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_400", {
        get: function () { return new color_1.Color("#FF7043").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_500", {
        get: function () { return new color_1.Color("#FF5722").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_600", {
        get: function () { return new color_1.Color("#F4511E").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_700", {
        get: function () { return new color_1.Color("#E64A19").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_800", {
        get: function () { return new color_1.Color("#D84315").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_900", {
        get: function () { return new color_1.Color("#BF360C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_A100", {
        get: function () { return new color_1.Color("#FF9E80").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_A200", {
        get: function () { return new color_1.Color("#FF6E40").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_A400", {
        get: function () { return new color_1.Color("#FF3D00").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_deep_orange_A700", {
        get: function () { return new color_1.Color("#DD2600").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_50", {
        get: function () { return new color_1.Color("#EFEBE9").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_100", {
        get: function () { return new color_1.Color("#D7CCC8").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_200", {
        get: function () { return new color_1.Color("#BCAAA4").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_300", {
        get: function () { return new color_1.Color("#A1887F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_400", {
        get: function () { return new color_1.Color("#8D6E63").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_500", {
        get: function () { return new color_1.Color("#795548").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_600", {
        get: function () { return new color_1.Color("#6D4C41").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_700", {
        get: function () { return new color_1.Color("#5D4037").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_800", {
        get: function () { return new color_1.Color("#4E342E").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_brown_900", {
        get: function () { return new color_1.Color("#3E2723").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_50", {
        get: function () { return new color_1.Color("#FAFAFA").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_100", {
        get: function () { return new color_1.Color("#F5F5F5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_200", {
        get: function () { return new color_1.Color("#EEEEEE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_300", {
        get: function () { return new color_1.Color("#E0E0E0").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_400", {
        get: function () { return new color_1.Color("#BDBDBD").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_500", {
        get: function () { return new color_1.Color("#9E9E9E").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_600", {
        get: function () { return new color_1.Color("#757575").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_700", {
        get: function () { return new color_1.Color("#616161").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_800", {
        get: function () { return new color_1.Color("#424242").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_grey_900", {
        get: function () { return new color_1.Color("#212121").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_black_1000", {
        get: function () { return new color_1.Color("#000000").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_white_1000", {
        get: function () { return new color_1.Color("#ffffff").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_50", {
        get: function () { return new color_1.Color("#ECEFF1").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_100", {
        get: function () { return new color_1.Color("#CFD8DC").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_200", {
        get: function () { return new color_1.Color("#B0BBC5").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_300", {
        get: function () { return new color_1.Color("#90A4AE").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_400", {
        get: function () { return new color_1.Color("#78909C").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_500", {
        get: function () { return new color_1.Color("#607D8B").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_600", {
        get: function () { return new color_1.Color("#546E7A").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_700", {
        get: function () { return new color_1.Color("#455A64").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_800", {
        get: function () { return new color_1.Color("#37474F").android; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Colors.prototype, "md_blue_grey_900", {
        get: function () { return new color_1.Color("#263238").android; },
        enumerable: true,
        configurable: true
    });
    return Colors;
}());
exports.Colors = Colors;
//# sourceMappingURL=colors.js.map