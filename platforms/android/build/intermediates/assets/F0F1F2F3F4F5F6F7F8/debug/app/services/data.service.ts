import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as firebase from "nativescript-plugin-firebase";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs';
import * as fs from 'file-system';
import * as Toast from 'nativescript-toast';

const imagesFBPath = 'lutsk/images/';
const audiosFBPath = 'lutsk/audios/';

@Injectable()
export class DataService {
    currentItemIndex: number;
    items: any[] = [];
    route: any[] = [];
    appPath = fs.knownFolders.currentApp().path;
    itemsSubject = new Subject();
    distanceTemplate = {
        m: {
            uk: "м",
            en: "m"
        },
        km: {
            uk: "км",
            en: "km"
        },
        label: {
            uk: "Відстань",
            en: "Distance"
        },
        distanceToItem: {
            uk: "В процесі",
            en: "Calculating"
        }
    };

    constructor(private ngZone: NgZone, ) {
        this.currentItemIndex = 0;
        var self = this;
        firebase.init({
            //persist should be set to false as otherwise numbers aren't returned during livesync
            persist: false,
            storageBucket: 'gs://ionic-tour.appspot.com',
        }).then(
            function (instance) {
                console.log("firebase.init done");
            },
            function (error) {
                console.log("firebase.init error: " + error);
            });
    }

    getFirstItem() {
        return this.items[0];
    }

    getNextItem() {
        let nextIndex = ++this.currentItemIndex;
        if (nextIndex >= this.items.length) {
            this.currentItemIndex = 0;
        } else {
            this.currentItemIndex = nextIndex;
        }
        return this.items[this.currentItemIndex];
    }

    getPrevItem() {
        let prevIndex = --this.currentItemIndex;
        if (prevIndex < 0) {
            this.currentItemIndex = this.items.length - 1;
        } else {
            this.currentItemIndex = prevIndex;
        }

        return this.items[this.currentItemIndex];
    }

    loadItemsList(): Observable<any> {
        return new Observable((observer: any) => {
            let path = 'items';
            let onValueEvent = (snapshot: any) => {
                this.ngZone.run(() => {
                    observer.next(snapshot.value);
                });
            };
            firebase.addValueEventListener(onValueEvent, `/${path}`);
        })
    }

    loadRoutePoints(): Observable<any> {
        return new Observable((observer: any) => {
            let path = 'route';
            let onValueEvent = (snapshot: any) => {
                this.ngZone.run(() => {
                    observer.next(snapshot.value);
                });
            };
            firebase.addValueEventListener(onValueEvent, `/${path}`);
        })
    }

    loadData(callback: any) {
        this.loadItemsList().subscribe(items => {
            this.ngZone.run(() => {
                this.items = items;
                this.itemsSubject.next(items);
                callback(true);
            })
        });
        this.loadRoutePoints().subscribe(routePoints => {
            this.ngZone.run(() => {
                this.route = routePoints;
            })
        })
    }

    downloadFile(filename: string, callback: any) {
        let localFullPath = this.appPath + '/' + filename;
        firebase.downloadFile({
            remoteFullPath: imagesFBPath + filename,
            localFullPath: localFullPath
        })
            .then(() => {
                callback(localFullPath)
            })
    }

    getAudioUrl(filename: string, callback) {
        firebase.getDownloadUrl({
            remoteFullPath: audiosFBPath + filename
        })
            .then(
            function (url) {
                callback(url)
            },
            function (error: string) {
                if(error.indexOf("Object does not exist at location.")>-1){
                    Toast.makeText("На жаль, для даного об'єкту немає аудіо").show();
                }
            });
    }

    fileExists(filename: string): boolean {
        return fs.File.exists(this.appPath + '/' + filename);
    }

    getFullFilePath(filename: string): string {
        return this.appPath + '/' + filename;
    }
}