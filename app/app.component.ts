import { Component, OnInit } from "@angular/core";
import { ParallaxView } from 'nativescript-ng2-parallax/nativescript-ng2-parallax';
import { Page } from "ui/page";
import { Button } from "ui/button";
import { DataService } from './services/data.service';
import * as geo from 'nativescript-geolocation';
import { NSMaterialShowcaseView } from 'nativescript-material-showcaseview';
import * as elementRegistryModule from 'nativescript-angular/element-registry';
import * as Toast from 'nativescript-toast';
import * as platform from 'platform';
import { Subject } from 'rxjs';
import * as fs from 'file-system';
import { TNSPlayer } from 'nativescript-audio';
import { TNSFontIconService } from 'nativescript-ng2-fonticon';
import * as application from 'application';
import * as dialogs from "ui/dialogs";


elementRegistryModule.registerElement("CardView", () => require("nativescript-cardview").CardView);
elementRegistryModule.registerElement("Gif", () => require("nativescript-gif").Gif);


@Component({
	selector: "my-app",
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
	lang: string = 'uk';
	currentItem: any;
	map: any;
	geoWatcher: any = null;
	distanceTemplate: any;
	distanceToItem: string;
	private showcaseView: NSMaterialShowcaseView;
	mapHeight: number = platform.screen.mainScreen.heightPixels >= 1280 ? 400 : platform.screen.mainScreen.heightPixels * 0.33;
	dataLoaded: boolean = false;
	itemImageLoaded: boolean = false;
	currentItemImage: string;
	itemsSubscription: any;
	audioWorker = new Worker('./workers/audio-player');
	audioIsPlaying: boolean = false;
	audioIsPaused: boolean = false;
	audioProgressValue: number = 0;

	constructor(private page: Page, private data: DataService, private fonticon: TNSFontIconService) {
		page.actionBarHidden = true;
		this.distanceTemplate = this.data.distanceTemplate;
		this.distanceToItem = this.distanceTemplate.distanceToItem[this.lang];
		this.data.loadData((isDataLoaded) => { this.dataLoaded = isDataLoaded; });
		this.showcaseView = new NSMaterialShowcaseView();
		this.data.itemsSubject.subscribe((res) => {
			this.currentItem = this.data.getFirstItem();
			this.handleItemImage();
			this.drawOnMapInit();
		});
		application.on(application.suspendEvent, (args: application.ApplicationEventData) => {
			this.resetPlayer();
		});
		this.audioWorker.onmessage = (msg) => {
			if ('currentProgress' in msg.data) {
				this.audioProgressValue = msg.data.currentProgress;
			}
		}
	}

	ngOnInit() {
		let options = {
			title: "Select language",
			message: "Choose your language",
			// cancelButtonText: "Cancel",
			actions: ["English", "Ukrainian"]
		};
		dialogs.action(options).then((result: string) => {
			this.lang = result === 'Ukrainian' ? 'uk' : 'en';
			this.showHelp();
		});
	}

	prevItem() {
		this.changeItem(this.data.getPrevItem());
	}

	nextItem() {
		this.changeItem(this.data.getNextItem());
	}

	changeItem(newItem: any) {
		this.itemImageLoaded = false;
		this.currentItem = newItem;
		this.handleItemImage();
		this.resetPlayer();
		this.showDistanceToItem();
		this.animateToItem();
	}

	handleItemImage() {
		if (this.data.fileExists(this.currentItem.image)) {
			this.currentItemImage = this.data.getFullFilePath(this.currentItem.image);
			this.itemImageLoaded = true;
		} else {
			this.data.downloadFile(this.currentItem.image, (file) => {
				this.currentItemImage = file;
				this.itemImageLoaded = true;
			});
		}
	}

	onMapReady(args: any) {
		this.map = args.map;
		geo.enableLocationRequest().then(res => {
			if (geo.isEnabled()) {
				this.geoWatcher = setInterval(() => this.showDistanceToItem, 60 * 1000)
			}
		})
	}

	drawOnMapInit() {
		if (this.map) {
			var onCalloutTap = function (marker) {
				alert("Marker callout tapped with title: '" + marker.title + "'");
			};
			this.map.addMarkers(this.data.items.map(v => {
				return {
					lat: v.coords.lat,
					lng: v.coords.lng,
					title: v.title[this.lang],
					// onCalloutTap: onCalloutTap
				}
			}));
			this.showDistanceToItem();
			this.drawRoute();
			this.map.setTilt(
				{
					tilt: 90, // default 30 (degrees angle)
					duration: 3000 // default 5000 (milliseconds)
				}
			)
		}
	}

	animateToItem() {
		this.map.animateCamera({
			target: {
				lat: this.currentItem.coords.lat,
				lng: this.currentItem.coords.lng,
			},
			zoomLevel: 17, // Android
			altitude: 2000, // iOS (meters from the ground)
			bearing: 270, // Where the camera is pointing, 0-360 (degrees)
			tilt: 50,
			duration: 1000 // in milliseconds
		})
	}

	drawRoute() {
		this.map.addPolyline({
			id: 1, // optional, can be used in 'removePolylines'
			color: '#336699', // Set the color of the line (default black)
			width: 7, //Set the width of the line (default 5)
			points: this.data.route
		});
	}

	showDistanceToItem() {
		try {
			if (geo.isEnabled()) {
				geo.getCurrentLocation({ desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000 })
					.then(currentLocation => {
						let itemLocation = {
							latitude: this.currentItem.coords.lat,
							longitude: this.currentItem.coords.lng,
							altitude: 181,
							horizontalAccuracy: 10,
							verticalAccuracy: 10,
							speed: 2,
							direction: 0,
							timestamp: new Date(),
							android: null,
							ios: null
						};
						let distanceInMeters = geo.distance(currentLocation, itemLocation);
						// Toast.makeText("distanceInMeters = " +distanceInMeters).show();
						this.distanceToItem = "" + (distanceInMeters > 1001 ?
							+ (distanceInMeters / 1000).toFixed(2)
							+ " " + this.distanceTemplate.km[this.lang] : distanceInMeters.toFixed(0)
							+ " " + this.distanceTemplate.m[this.lang]);
					})
					.catch(error => {
						console.log("getCurrentLocation error:", error);
					})
			}
		}
		catch (e) {
			Toast.makeText(JSON.stringify(e)).show();
		}
	}

	showHelp() {
		this.showcaseView.createSequence(
			[
				{
					target: this.page.getViewById("map"),
					dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
					contentText: this.lang === "uk" ? "Це карта, де показані туристичні об'єкти маршруту" : "This is map, where tourist items will be shown.",
					withRectangleShape: false
				},
				// {
				// 	target: this.page.getViewById("play_button"),
				// 	dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
				// 	contentText: this.lang === "uk" ? "Ця кнопка дозволяє прослухати аудіогід." : "This button let you listen audioguide.",
				// 	withRectangleShape: false
				// },
				{
					target: this.page.getViewById("content"),
					dismissText: this.lang === "uk" ? "Зрозуміло" : "GOT IT",
					contentText: this.lang === "uk" ? "Тут відображається інформація про туристичний об'єкт." : "Here you can see info about tourist item.",
					withRectangleShape: false
				},
			]);

		this.showcaseView.startSequence();
		// this.showcaseView.reset();
	}

	playPause() {
		if (!this.audioIsPlaying && !this.audioIsPaused) {
			this.data.getAudioUrl(this.currentItem.audio[this.lang], (url) => {
				this.audioIsPlaying = true;
				this.audioWorker.postMessage({ url: url, action: "play" });
			})
		} else if (!this.audioIsPlaying && this.audioIsPaused) {
			this.audioWorker.postMessage({ action: "resume" });
			this.audioIsPlaying = true;
			this.audioIsPaused = false;
		} else if (this.audioIsPlaying) {
			this.audioWorker.postMessage({ action: "pause" });
			this.audioIsPlaying = false;
			this.audioIsPaused = true;
		}
	}

	resetPlayer() {
		this.audioIsPlaying = false;
		this.audioIsPaused = false;
		this.audioProgressValue = 0;
		this.audioWorker.postMessage({ action: "reset" });
	}
}

