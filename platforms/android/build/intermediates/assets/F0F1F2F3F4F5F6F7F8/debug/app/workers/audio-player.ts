require("globals");

import { TNSPlayer } from 'nativescript-audio';
const player = new TNSPlayer();
var timePoster;

const setPlayer = () => {
    timePoster = setInterval(() => {
        if (player.isAudioPlaying) {
            player.getAudioTrackDuration()
                .then(duration => {
                    let trackDuration = parseInt(duration);
                    if (!isNaN(trackDuration) && trackDuration > 0) {
                        global.postMessage({ currentProgress: Math.floor(player.currentTime / trackDuration * 100) })
                    }
                })
        }
    }, 500);
}

const resetPlayer = () => { 
    player.pause()
    clearInterval(timePoster);
}

global.onmessage = (msg) => {
    let action = msg.data.action;
    if (action === 'play') {
        setPlayer();
        player.playFromUrl({
            audioFile: msg.data.url,
            loop: false
        }).then(() => {
            console.log("pPlaying started")
        })
    } else if (action === 'pause') {
        player.pause();
    } else if (action === 'resume') {
        player.resume();
    } else if (action === 'reset') {
        resetPlayer();
    }
};

