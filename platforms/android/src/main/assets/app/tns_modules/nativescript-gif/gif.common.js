"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var view = require("ui/core/view");
var proxy_1 = require("ui/core/proxy");
var dependencyObservable = require("ui/core/dependency-observable");
var platform = require("platform");
var types = require("utils/types");
var SRC = "src";
var GIF = "Gif";
var ISLOADING = "isLoading";
var AffectsLayout = platform.device.os === platform.platformNames.android ? dependencyObservable.PropertyMetadataSettings.None : dependencyObservable.PropertyMetadataSettings.AffectsLayout;
function onSrcPropertyChanged(data) {
    var gif = data.object;
    var value = data.newValue;
    if (types.isString(value)) {
        value = value.trim();
        gif.src = value;
    }
}
var Gif = (function (_super) {
    __extends(Gif, _super);
    function Gif(options) {
        return _super.call(this, options) || this;
    }
    Object.defineProperty(Gif.prototype, "src", {
        get: function () {
            return this._getValue(Gif.srcProperty);
        },
        set: function (value) {
            this._setValue(Gif.srcProperty, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Gif.prototype, "isLoading", {
        get: function () {
            return this._getValue(Gif.isLoadingProperty);
        },
        enumerable: true,
        configurable: true
    });
    return Gif;
}(view.View));
Gif.srcProperty = new dependencyObservable.Property(SRC, GIF, new proxy_1.PropertyMetadata(undefined, dependencyObservable.PropertyMetadataSettings.None, onSrcPropertyChanged));
Gif.isLoadingProperty = new dependencyObservable.Property(ISLOADING, GIF, new proxy_1.PropertyMetadata(false, dependencyObservable.PropertyMetadataSettings.None));
exports.Gif = Gif;
//# sourceMappingURL=gif.common.js.map