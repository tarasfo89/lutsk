"use strict";
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var firebase = require("nativescript-plugin-firebase");
var rxjs_1 = require("rxjs");
var fs = require("file-system");
var Toast = require("nativescript-toast");
var imagesFBPath = 'lutsk/images/';
var audiosFBPath = 'lutsk/audios/';
var DataService = (function () {
    function DataService(ngZone) {
        this.ngZone = ngZone;
        this.items = [];
        this.route = [];
        this.appPath = fs.knownFolders.currentApp().path;
        this.itemsSubject = new rxjs_1.Subject();
        this.distanceTemplate = {
            m: {
                uk: "м",
                en: "m"
            },
            km: {
                uk: "км",
                en: "km"
            },
            label: {
                uk: "Відстань",
                en: "Distance"
            },
            distanceToItem: {
                uk: "В процесі",
                en: "Calculating"
            }
        };
        this.currentItemIndex = 0;
        var self = this;
        firebase.init({
            //persist should be set to false as otherwise numbers aren't returned during livesync
            persist: false,
            storageBucket: 'gs://ionic-tour.appspot.com',
        }).then(function (instance) {
            console.log("firebase.init done");
        }, function (error) {
            console.log("firebase.init error: " + error);
        });
    }
    DataService.prototype.getFirstItem = function () {
        return this.items[0];
    };
    DataService.prototype.getNextItem = function () {
        var nextIndex = ++this.currentItemIndex;
        if (nextIndex >= this.items.length) {
            this.currentItemIndex = 0;
        }
        else {
            this.currentItemIndex = nextIndex;
        }
        return this.items[this.currentItemIndex];
    };
    DataService.prototype.getPrevItem = function () {
        var prevIndex = --this.currentItemIndex;
        if (prevIndex < 0) {
            this.currentItemIndex = this.items.length - 1;
        }
        else {
            this.currentItemIndex = prevIndex;
        }
        return this.items[this.currentItemIndex];
    };
    DataService.prototype.loadItemsList = function () {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            var path = 'items';
            var onValueEvent = function (snapshot) {
                _this.ngZone.run(function () {
                    observer.next(snapshot.value);
                });
            };
            firebase.addValueEventListener(onValueEvent, "/" + path);
        });
    };
    DataService.prototype.loadRoutePoints = function () {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            var path = 'route';
            var onValueEvent = function (snapshot) {
                _this.ngZone.run(function () {
                    observer.next(snapshot.value);
                });
            };
            firebase.addValueEventListener(onValueEvent, "/" + path);
        });
    };
    DataService.prototype.loadData = function (callback) {
        var _this = this;
        this.loadItemsList().subscribe(function (items) {
            _this.ngZone.run(function () {
                _this.items = items;
                _this.itemsSubject.next(items);
                callback(true);
            });
        });
        this.loadRoutePoints().subscribe(function (routePoints) {
            _this.ngZone.run(function () {
                _this.route = routePoints;
            });
        });
    };
    DataService.prototype.downloadFile = function (filename, callback) {
        var localFullPath = this.appPath + '/' + filename;
        firebase.downloadFile({
            remoteFullPath: imagesFBPath + filename,
            localFullPath: localFullPath
        })
            .then(function () {
            callback(localFullPath);
        });
    };
    DataService.prototype.getAudioUrl = function (filename, callback) {
        firebase.getDownloadUrl({
            remoteFullPath: audiosFBPath + filename
        })
            .then(function (url) {
            callback(url);
        }, function (error) {
            if (error.indexOf("Object does not exist at location.") > -1) {
                Toast.makeText("На жаль, для даного об'єкту немає аудіо").show();
            }
        });
    };
    DataService.prototype.fileExists = function (filename) {
        return fs.File.exists(this.appPath + '/' + filename);
    };
    DataService.prototype.getFullFilePath = function (filename) {
        return this.appPath + '/' + filename;
    };
    return DataService;
}());
DataService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [core_1.NgZone])
], DataService);
exports.DataService = DataService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxzQ0FBbUQ7QUFDbkQsOENBQTZDO0FBQzdDLHVEQUF5RDtBQUV6RCw2QkFBK0I7QUFDL0IsZ0NBQWtDO0FBQ2xDLDBDQUE0QztBQUU1QyxJQUFNLFlBQVksR0FBRyxlQUFlLENBQUM7QUFDckMsSUFBTSxZQUFZLEdBQUcsZUFBZSxDQUFDO0FBR3JDLElBQWEsV0FBVztJQXlCcEIscUJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBdkJsQyxVQUFLLEdBQVUsRUFBRSxDQUFDO1FBQ2xCLFVBQUssR0FBVSxFQUFFLENBQUM7UUFDbEIsWUFBTyxHQUFHLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDO1FBQzVDLGlCQUFZLEdBQUcsSUFBSSxjQUFPLEVBQUUsQ0FBQztRQUM3QixxQkFBZ0IsR0FBRztZQUNmLENBQUMsRUFBRTtnQkFDQyxFQUFFLEVBQUUsR0FBRztnQkFDUCxFQUFFLEVBQUUsR0FBRzthQUNWO1lBQ0QsRUFBRSxFQUFFO2dCQUNBLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2FBQ1g7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsRUFBRSxFQUFFLFVBQVU7Z0JBQ2QsRUFBRSxFQUFFLFVBQVU7YUFDakI7WUFDRCxjQUFjLEVBQUU7Z0JBQ1osRUFBRSxFQUFFLFdBQVc7Z0JBQ2YsRUFBRSxFQUFFLGFBQWE7YUFDcEI7U0FDSixDQUFDO1FBR0UsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUMxQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNWLHFGQUFxRjtZQUNyRixPQUFPLEVBQUUsS0FBSztZQUNkLGFBQWEsRUFBRSw2QkFBNkI7U0FDL0MsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVLFFBQVE7WUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUNELFVBQVUsS0FBSztZQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDakQsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsa0NBQVksR0FBWjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxpQ0FBVyxHQUFYO1FBQ0ksSUFBSSxTQUFTLEdBQUcsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDeEMsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7UUFDdEMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxpQ0FBVyxHQUFYO1FBQ0ksSUFBSSxTQUFTLEdBQUcsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDeEMsRUFBRSxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNsRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1FBQ3RDLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsbUNBQWEsR0FBYjtRQUFBLGlCQVVDO1FBVEcsTUFBTSxDQUFDLElBQUksdUJBQVUsQ0FBQyxVQUFDLFFBQWE7WUFDaEMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDO1lBQ25CLElBQUksWUFBWSxHQUFHLFVBQUMsUUFBYTtnQkFDN0IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7b0JBQ1osUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDO1lBQ0YsUUFBUSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxNQUFJLElBQU0sQ0FBQyxDQUFDO1FBQzdELENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELHFDQUFlLEdBQWY7UUFBQSxpQkFVQztRQVRHLE1BQU0sQ0FBQyxJQUFJLHVCQUFVLENBQUMsVUFBQyxRQUFhO1lBQ2hDLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQztZQUNuQixJQUFJLFlBQVksR0FBRyxVQUFDLFFBQWE7Z0JBQzdCLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO29CQUNaLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQztZQUNGLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsTUFBSSxJQUFNLENBQUMsQ0FBQztRQUM3RCxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCw4QkFBUSxHQUFSLFVBQVMsUUFBYTtRQUF0QixpQkFhQztRQVpHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLO1lBQ2hDLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNaLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2dCQUNuQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDOUIsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25CLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsV0FBVztZQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDWixLQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELGtDQUFZLEdBQVosVUFBYSxRQUFnQixFQUFFLFFBQWE7UUFDeEMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDO1FBQ2xELFFBQVEsQ0FBQyxZQUFZLENBQUM7WUFDbEIsY0FBYyxFQUFFLFlBQVksR0FBRyxRQUFRO1lBQ3ZDLGFBQWEsRUFBRSxhQUFhO1NBQy9CLENBQUM7YUFDRyxJQUFJLENBQUM7WUFDRixRQUFRLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDM0IsQ0FBQyxDQUFDLENBQUE7SUFDVixDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLFFBQWdCLEVBQUUsUUFBUTtRQUNsQyxRQUFRLENBQUMsY0FBYyxDQUFDO1lBQ3BCLGNBQWMsRUFBRSxZQUFZLEdBQUcsUUFBUTtTQUMxQyxDQUFDO2FBQ0csSUFBSSxDQUNMLFVBQVUsR0FBRztZQUNULFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNqQixDQUFDLEVBQ0QsVUFBVSxLQUFhO1lBQ25CLEVBQUUsQ0FBQSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsb0NBQW9DLENBQUMsR0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQ3ZELEtBQUssQ0FBQyxRQUFRLENBQUMseUNBQXlDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNyRSxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsZ0NBQVUsR0FBVixVQUFXLFFBQWdCO1FBQ3ZCLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxRQUFRLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQscUNBQWUsR0FBZixVQUFnQixRQUFnQjtRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDO0lBQ3pDLENBQUM7SUFDTCxrQkFBQztBQUFELENBQUMsQUExSUQsSUEwSUM7QUExSVksV0FBVztJQUR2QixpQkFBVSxFQUFFO3FDQTBCbUIsYUFBTTtHQXpCekIsV0FBVyxDQTBJdkI7QUExSVksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBOZ1pvbmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0ICogYXMgZmlyZWJhc2UgZnJvbSBcIm5hdGl2ZXNjcmlwdC1wbHVnaW4tZmlyZWJhc2VcIjtcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMvQmVoYXZpb3JTdWJqZWN0JztcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIGZzIGZyb20gJ2ZpbGUtc3lzdGVtJztcbmltcG9ydCAqIGFzIFRvYXN0IGZyb20gJ25hdGl2ZXNjcmlwdC10b2FzdCc7XG5cbmNvbnN0IGltYWdlc0ZCUGF0aCA9ICdsdXRzay9pbWFnZXMvJztcbmNvbnN0IGF1ZGlvc0ZCUGF0aCA9ICdsdXRzay9hdWRpb3MvJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGFTZXJ2aWNlIHtcbiAgICBjdXJyZW50SXRlbUluZGV4OiBudW1iZXI7XG4gICAgaXRlbXM6IGFueVtdID0gW107XG4gICAgcm91dGU6IGFueVtdID0gW107XG4gICAgYXBwUGF0aCA9IGZzLmtub3duRm9sZGVycy5jdXJyZW50QXBwKCkucGF0aDtcbiAgICBpdGVtc1N1YmplY3QgPSBuZXcgU3ViamVjdCgpO1xuICAgIGRpc3RhbmNlVGVtcGxhdGUgPSB7XG4gICAgICAgIG06IHtcbiAgICAgICAgICAgIHVrOiBcItC8XCIsXG4gICAgICAgICAgICBlbjogXCJtXCJcbiAgICAgICAgfSxcbiAgICAgICAga206IHtcbiAgICAgICAgICAgIHVrOiBcItC60LxcIixcbiAgICAgICAgICAgIGVuOiBcImttXCJcbiAgICAgICAgfSxcbiAgICAgICAgbGFiZWw6IHtcbiAgICAgICAgICAgIHVrOiBcItCS0ZbQtNGB0YLQsNC90YxcIixcbiAgICAgICAgICAgIGVuOiBcIkRpc3RhbmNlXCJcbiAgICAgICAgfSxcbiAgICAgICAgZGlzdGFuY2VUb0l0ZW06IHtcbiAgICAgICAgICAgIHVrOiBcItCSINC/0YDQvtGG0LXRgdGWXCIsXG4gICAgICAgICAgICBlbjogXCJDYWxjdWxhdGluZ1wiXG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSwgKSB7XG4gICAgICAgIHRoaXMuY3VycmVudEl0ZW1JbmRleCA9IDA7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgZmlyZWJhc2UuaW5pdCh7XG4gICAgICAgICAgICAvL3BlcnNpc3Qgc2hvdWxkIGJlIHNldCB0byBmYWxzZSBhcyBvdGhlcndpc2UgbnVtYmVycyBhcmVuJ3QgcmV0dXJuZWQgZHVyaW5nIGxpdmVzeW5jXG4gICAgICAgICAgICBwZXJzaXN0OiBmYWxzZSxcbiAgICAgICAgICAgIHN0b3JhZ2VCdWNrZXQ6ICdnczovL2lvbmljLXRvdXIuYXBwc3BvdC5jb20nLFxuICAgICAgICB9KS50aGVuKFxuICAgICAgICAgICAgZnVuY3Rpb24gKGluc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJmaXJlYmFzZS5pbml0IGRvbmVcIik7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJmaXJlYmFzZS5pbml0IGVycm9yOiBcIiArIGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGdldEZpcnN0SXRlbSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXRlbXNbMF07XG4gICAgfVxuXG4gICAgZ2V0TmV4dEl0ZW0oKSB7XG4gICAgICAgIGxldCBuZXh0SW5kZXggPSArK3RoaXMuY3VycmVudEl0ZW1JbmRleDtcbiAgICAgICAgaWYgKG5leHRJbmRleCA+PSB0aGlzLml0ZW1zLmxlbmd0aCkge1xuICAgICAgICAgICAgdGhpcy5jdXJyZW50SXRlbUluZGV4ID0gMDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuY3VycmVudEl0ZW1JbmRleCA9IG5leHRJbmRleDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtc1t0aGlzLmN1cnJlbnRJdGVtSW5kZXhdO1xuICAgIH1cblxuICAgIGdldFByZXZJdGVtKCkge1xuICAgICAgICBsZXQgcHJldkluZGV4ID0gLS10aGlzLmN1cnJlbnRJdGVtSW5kZXg7XG4gICAgICAgIGlmIChwcmV2SW5kZXggPCAwKSB7XG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRJdGVtSW5kZXggPSB0aGlzLml0ZW1zLmxlbmd0aCAtIDE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRJdGVtSW5kZXggPSBwcmV2SW5kZXg7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtc1t0aGlzLmN1cnJlbnRJdGVtSW5kZXhdO1xuICAgIH1cblxuICAgIGxvYWRJdGVtc0xpc3QoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcjogYW55KSA9PiB7XG4gICAgICAgICAgICBsZXQgcGF0aCA9ICdpdGVtcyc7XG4gICAgICAgICAgICBsZXQgb25WYWx1ZUV2ZW50ID0gKHNuYXBzaG90OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNuYXBzaG90LnZhbHVlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBmaXJlYmFzZS5hZGRWYWx1ZUV2ZW50TGlzdGVuZXIob25WYWx1ZUV2ZW50LCBgLyR7cGF0aH1gKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBsb2FkUm91dGVQb2ludHMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcjogYW55KSA9PiB7XG4gICAgICAgICAgICBsZXQgcGF0aCA9ICdyb3V0ZSc7XG4gICAgICAgICAgICBsZXQgb25WYWx1ZUV2ZW50ID0gKHNuYXBzaG90OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNuYXBzaG90LnZhbHVlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBmaXJlYmFzZS5hZGRWYWx1ZUV2ZW50TGlzdGVuZXIob25WYWx1ZUV2ZW50LCBgLyR7cGF0aH1gKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBsb2FkRGF0YShjYWxsYmFjazogYW55KSB7XG4gICAgICAgIHRoaXMubG9hZEl0ZW1zTGlzdCgpLnN1YnNjcmliZShpdGVtcyA9PiB7XG4gICAgICAgICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuaXRlbXMgPSBpdGVtcztcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1zU3ViamVjdC5uZXh0KGl0ZW1zKTtcbiAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmxvYWRSb3V0ZVBvaW50cygpLnN1YnNjcmliZShyb3V0ZVBvaW50cyA9PiB7XG4gICAgICAgICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMucm91dGUgPSByb3V0ZVBvaW50cztcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZG93bmxvYWRGaWxlKGZpbGVuYW1lOiBzdHJpbmcsIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgbGV0IGxvY2FsRnVsbFBhdGggPSB0aGlzLmFwcFBhdGggKyAnLycgKyBmaWxlbmFtZTtcbiAgICAgICAgZmlyZWJhc2UuZG93bmxvYWRGaWxlKHtcbiAgICAgICAgICAgIHJlbW90ZUZ1bGxQYXRoOiBpbWFnZXNGQlBhdGggKyBmaWxlbmFtZSxcbiAgICAgICAgICAgIGxvY2FsRnVsbFBhdGg6IGxvY2FsRnVsbFBhdGhcbiAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjayhsb2NhbEZ1bGxQYXRoKVxuICAgICAgICAgICAgfSlcbiAgICB9XG5cbiAgICBnZXRBdWRpb1VybChmaWxlbmFtZTogc3RyaW5nLCBjYWxsYmFjaykge1xuICAgICAgICBmaXJlYmFzZS5nZXREb3dubG9hZFVybCh7XG4gICAgICAgICAgICByZW1vdGVGdWxsUGF0aDogYXVkaW9zRkJQYXRoICsgZmlsZW5hbWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKFxuICAgICAgICAgICAgZnVuY3Rpb24gKHVybCkge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKHVybClcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBmdW5jdGlvbiAoZXJyb3I6IHN0cmluZykge1xuICAgICAgICAgICAgICAgIGlmKGVycm9yLmluZGV4T2YoXCJPYmplY3QgZG9lcyBub3QgZXhpc3QgYXQgbG9jYXRpb24uXCIpPi0xKXtcbiAgICAgICAgICAgICAgICAgICAgVG9hc3QubWFrZVRleHQoXCLQndCwINC20LDQu9GMLCDQtNC70Y8g0LTQsNC90L7Qs9C+INC+0LEn0ZTQutGC0YMg0L3QtdC80LDRlCDQsNGD0LTRltC+XCIpLnNob3coKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmaWxlRXhpc3RzKGZpbGVuYW1lOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIGZzLkZpbGUuZXhpc3RzKHRoaXMuYXBwUGF0aCArICcvJyArIGZpbGVuYW1lKTtcbiAgICB9XG5cbiAgICBnZXRGdWxsRmlsZVBhdGgoZmlsZW5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcFBhdGggKyAnLycgKyBmaWxlbmFtZTtcbiAgICB9XG59Il19