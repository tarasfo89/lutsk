import { ParallaxView } from './src/app/parallax.component';
export * from './src/app/parallax.component';
declare var _default: {
    directives: typeof ParallaxView[];
};
export default _default;
