var path = require('path');
var fs = require('fs-extra');
var files = [
    [path.resolve('./', 'platforms/android/source/SplashScreen.js'), path.resolve('../../', 'app/SplashScreen.js')],
    [path.resolve('./', 'platforms/android/fonts/RobotoRegular.ttf'), path.resolve('../../', 'app/fonts/RobotoRegular.ttf')],
    [path.resolve('./', 'platforms/android/res/drawable/splash_logo.png'), path.resolve('../../', 'app/App_Resources/Android/drawable/splash_logo.png')]
];
files.forEach(function (item, index) {
    var fileOrFolder = item[0];
    var dest = item[1];
    fs.copy(fileOrFolder, "" + dest, { clobber: false }, function (err) {
        if (err)
            return console.error(err);
    });
});
//# sourceMappingURL=install.js.map