import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { ParallaxView } from 'nativescript-ng2-parallax/nativescript-ng2-parallax';
import { AppComponent } from "./app.component";
import { DataService } from './services/data.service';
import { TNSFontIconModule } from 'nativescript-ng2-fonticon';
import { registerElement } from 'nativescript-angular/element-registry';
var map = require("nativescript-mapbox");
registerElement("Mapbox", () => map.Mapbox);

@NgModule({
  declarations: [AppComponent, ParallaxView, ],
  bootstrap: [AppComponent],
  imports: [NativeScriptModule,
    TNSFontIconModule.forRoot({
      'fa': './fonts/font-awesome.min.css'
    }),
    ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [DataService]
})
export class AppModule { }
