"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
// for standard export at bottom
var parallax_component_1 = require('./src/app/parallax.component');
__export(require('./src/app/parallax.component'));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    directives: [parallax_component_1.ParallaxView]
};
//# sourceMappingURL=nativescript-ng2-parallax.js.map